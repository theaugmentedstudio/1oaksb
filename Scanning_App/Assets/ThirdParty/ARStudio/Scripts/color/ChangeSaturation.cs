﻿using UnityEngine;
using System;
using System.Collections;

public class ChangeSaturation : MonoBehaviour {


	/**
 	 * -------------------------------------- PROPERTIES----------------------------------------
	 */

	// inspector
	public GameObject[] selfIlum;
	public Light sun;
	public float maxSunIntensity;


	/**
 	 * -------------------------------------- updateSaturation ----------------------------------------
	 */

	public void UpdateSaturation ( float value )
	{
		sun.intensity = value*maxSunIntensity;
		foreach (var item in selfIlum) {
			item.GetComponent<Renderer>().material.color = new Color(value, value, value);
		}
	}

}
