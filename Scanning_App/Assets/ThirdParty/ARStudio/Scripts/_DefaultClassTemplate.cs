//------------------------------------------------------------------------------
// Copyright Anthony De Brackeleire 2014
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class DefaultClassTemplate : MonoBehaviour
{
	/* -------------------------------------- PROPERTIES ---------------------------------------- */

	// INSPECTOR 

	// DATA

 	/* -------------------------------------- MONOBEHAVIOR ---------------------------------------- */

	void Awake(){ init ();}
	void Start(){}
	void Update(){}
	
	/* -------------------------------------- INIT ---------------------------------------- */

	void init()
	{
	}
	
}


