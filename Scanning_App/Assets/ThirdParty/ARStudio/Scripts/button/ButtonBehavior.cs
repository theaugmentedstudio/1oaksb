﻿using UnityEngine;
using System.Collections;

public class ButtonBehavior : MonoBehaviour
{
	// public events
	public delegate void buttonHandler( GameObject obj );
	public event buttonHandler onMouseUp;
	public event buttonHandler onMouseDown;
	
	// gui
	public Texture2D downTexture;
	
	// vars
	Texture2D upTexture;
	GUITexture tex ;
	string state = "up";
	
	
	// Use this for initialization
	void Start () {
		tex = GetComponent<GUITexture>();
		if(tex){
			upTexture = tex.texture as Texture2D;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!tex) return;
		
		/*
		for (int i = 0; i < Input.touchCount; ++i)
	    {
			Console.Log("hit!");
	        //if (Input.GetTouch(i).phase == TouchPhase.Began) 
		}	
		*/
		
		
		if(Input.GetMouseButtonDown(0))
		{
			if (tex.HitTest(Input.mousePosition)){
				state = "down";
				updateTexture();
				if(onMouseDown != null) onMouseDown( gameObject );
			}
		}
		
		if(Input.GetMouseButtonUp(0))
		{
			if (state == "down")
			{
				if(tex.HitTest(Input.mousePosition))
				{
					if(onMouseUp != null) onMouseUp( gameObject );
				}
				state = "up";
				updateTexture();
			}
		}
		
		/*
		if (tex.HitTest(Input.GetTouch(0).position))
    	{
			Console.Log("hit!");
		}
		
		foreach (Touch evt in Input.touches)
		{
			Console.Log("in touch");
			var HitTest1 = tex.HitTest(evt.position);		
			if (evt.phase == TouchPhase.Began) 
			{
				if(HitTest1){			
					//do something when button is hit
					Console.Log("ButtonGUI Clicked");
				}
			
			}
		}
		*/
	}
	
	void updateTexture()
	{
		if(state == "down") tex.texture = downTexture;
		else tex.texture = upTexture;
	}
}
 