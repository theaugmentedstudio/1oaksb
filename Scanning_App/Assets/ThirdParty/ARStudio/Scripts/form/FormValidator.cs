﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

public class FormValidator{

	public static bool isMailValid (string email)
	{
		string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" 
			+ @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" 
				+ @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
		
		Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
		return regex.IsMatch(email);
	}


			
	public static bool isMailValid2(string email)
	{
		string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
			+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
				+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}
}
