﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


[AddComponentMenu("Camera-Control/Mouse drag Orbit with zoom")]
public class DragMouseOrbit : MonoBehaviour
{

	/* -------------------------------------- PROPERTIES ---------------------------------------- */

	// inspector & public

	public Transform target;
	public float distance = 10.0f;
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	//public float yMinLimit = 0.0f;//-20f;
	//public float yMaxLimit = 80f;
	public float distanceMin = 5.0f;
	public float distanceMax = 15f;
	//public float smoothTime = 0.3f;

	// props
	float rotationYAxis = 0.0f;
	float rotationXAxis = 0.0f;
	//float velocityX = 0.0f;
	//float velocityY = 0.0f;

	bool isChangingTarget = false;


	/* -------------------------------------- MONO ---------------------------------------- */


	// Use this for initialization
	void Start()
	{
		Debug.Log ("Start");
		/*
		Vector3 angles = transform.eulerAngles;
		rotationYAxis = angles.y;
		rotationXAxis = angles.x;
		*/
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
		{
			GetComponent<Rigidbody>().freezeRotation = true;
		}
	}
	/*
	void LateUpdate()
	{
		if (target)
		{
			if (Input.GetMouseButton(0))
			{
				velocityX += xSpeed * Input.GetAxis("Mouse X") * distance * 0.002f;
				velocityY += ySpeed * Input.GetAxis("Mouse Y") * 0.002f;
			}
			rotationYAxis += velocityX;
			rotationXAxis -= velocityY;
			rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
			Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
			Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
			Quaternion rotation = toRotation;
			
			distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
			/*
			RaycastHit hit;
			if (Physics.Linecast(target.position, transform.position, out hit))
			{
				distance -= hit.distance;
			}
			* /
			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
			Vector3 position = rotation * negDistance + target.position;
			
			transform.rotation = rotation;
			transform.position = position;
			velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
			velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
		}
	}
*/


	void Update () 
	{
		if (target && !isChangingTarget) 
		{
			if (Input.GetMouseButton (0)) 
			{
				rotationYAxis += Input.GetAxis ("Mouse X") * xSpeed * distance * 0.02f;
				rotationXAxis -= Input.GetAxis ("Mouse Y") * ySpeed * 0.02f;
				
				//rotationXAxis = ClampAngle (rotationXAxis, yMinLimit, yMaxLimit); // limits
			}
			
			Quaternion rotation = Quaternion.Euler (rotationXAxis, rotationYAxis, 0);
			
			//distance = Mathf.Clamp (distance - Input.GetAxis ("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

			/*
			RaycastHit hit;
			if (Physics.Linecast (target.position, transform.position, out hit)) {
				distance -= hit.distance;
			}*/
			Vector3 negDistance = new Vector3 (0.0f, 0.0f, -distance);
			Vector3 position = rotation * negDistance + target.position;
			
			transform.rotation = rotation;
			transform.position = position;
		}
	}



	public void ChangeTarget( Transform newTarget )
	{
		Debug.Log ("ChangeTarget");
		isChangingTarget = true;

		// kill possible existing tweens
		DOTween.KillAll ();

		// update target
		target = newTarget;

		// rotation
		Vector3 relativePos = target.position - transform.position;
		Quaternion endRotation = Quaternion.LookRotation(relativePos);

		rotationXAxis = endRotation.eulerAngles.x;
		rotationYAxis = endRotation.eulerAngles.y;
		endRotation = Quaternion.Euler (rotationXAxis, rotationYAxis, 0);
		//rotationYAxis = Random.Range (0, 360); // random rotation to have a nice camera effect
		//Quaternion rotation = Quaternion.Euler (rotationXAxis, rotationYAxis, 0);


		// distance must be the half of max distance
		distance = distanceMax *0.8f;

		// new position
		Vector3 negDistance = new Vector3 (0.0f, 0.0f, -distance);
		Vector3 position = endRotation * negDistance + target.position;

		/*
		transform.rotation = rotation;
		transform.position = position;
		*/

		// tween position and rotation
		DOTween.To (() => transform.rotation, x => transform.rotation = x, endRotation.eulerAngles, 2.0f).SetId("camRotateTween").SetDelay(0.0f).SetEase(Ease.OutCubic).OnComplete(onTargetChanged);
		DOTween.To(()=> transform.position, x=> transform.position = x, position, 2.0f).SetDelay(0.0f).SetId("camPosTween").SetEase(Ease.OutCubic);

	}
	void onTargetChanged()
	{
		Debug.Log ("onTargetChanged");
		isChangingTarget = false;
	}



	/* -------------------------------------- Static helper ---------------------------------------- */


	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
}