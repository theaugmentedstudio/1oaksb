﻿using UnityEngine;
using System.Collections;


/**
 * http://forum.unity3d.com/threads/ui-animate-fullscreen-panel-from-off-screen.271751/
 * 
 * As an example, here's the script I made to do exactly as secondbreakfast gives as his second solution. 
 * Attach it to the UI object with the RectTransform that you'd like to animate, and animate the 'animatablePosition' float on this script instead of the position directly.

My script only works for full-screen slide-in's - not for animating not-so-full-screen things.

 */

public class AnimatableScreen : MonoBehaviour
{
	public enum Position
	{
		Center,
		Left,
		Right,
		Up,
		Down
	}
	
	public float animatablePosition = 0f;
	public Position positionAtZero = Position.Center;
	public Position positionAtOne = Position.Down;
	
	private RectTransform rectTransform;
	
	void Start ()
	{
		rectTransform = GetComponent<RectTransform>();
	}
	
	void Update ()
	{
		Vector2 posAt0 = GetPosition(positionAtZero);
		Vector2 posAt1 = GetPosition(positionAtOne);
		rectTransform.anchoredPosition =  Vector2.Lerp(posAt0, posAt1, animatablePosition);
	}
	
	Vector2 GetPosition(Position p)
	{
		switch(p)
		{
		case Position.Center: return Vector2.zero;
		case Position.Up: return new Vector2(0f, rectTransform.rect.height);
		case Position.Down: return new Vector2(0f , -rectTransform.rect.height);
		case Position.Left: return new Vector2(-rectTransform.rect.width, 0f);
		case Position.Right: return new Vector2(rectTransform.rect.width, 0f);
		default: return Vector2.zero;
		}
	}
}