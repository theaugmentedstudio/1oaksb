﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TweenUtil 
{


	/** -------------------------------------- MOVE TO (UI) ----------------------------------------*/

	public static Tween UIMoveTo( RectTransform rectTransform, Vector2 startPos, Vector2 endPos, float duration, float delay = 0.0f )
	{
		rectTransform.anchoredPosition = startPos;
		return DOTween.To(()=> rectTransform.anchoredPosition, x=> rectTransform.anchoredPosition = x, endPos, duration).SetDelay(delay).SetEase(Ease.OutCubic);
	}


	/** -------------------------------------- SCALE TO  ----------------------------------------*/

	
	public static Tween ScaleTo( GameObject gameObject, Vector3 startScale, Vector3 endScale, float duration, float delay = 0.0f )
	{
		gameObject.transform.localScale = startScale;
		return DOTween.To(()=> gameObject.transform.localScale, x=> gameObject.transform.localScale = x, endScale, duration).SetDelay(delay).SetEase(Ease.OutCubic);
	}


	/** -------------------------------------- MOVE TO  ----------------------------------------*/

	public static Tween MoveTo( GameObject GO, Vector3 startPos, Vector3 endPos, float duration, float delay = 0.0f )
	{
		GO.transform.position = startPos;
		return DOTween.To(()=> GO.transform.position, x=> GO.transform.position = x, endPos, duration).SetDelay(delay).SetEase(Ease.OutCubic);
	}

	public static Tween LocalMoveTo( GameObject GO, Vector3 startPos, Vector3 endPos, float duration, float delay = 0.0f )
	{
		GO.transform.localPosition = startPos;
		return DOTween.To(()=> GO.transform.localPosition, x=> GO.transform.localPosition = x, endPos, duration).SetDelay(delay).SetEase(Ease.OutCubic);
	}


	/** -------------------------------------- KiLL ----------------------------------------*/

	public static int KillTween(string tweenId)
	{
		return DOTween.Kill (tweenId);
	}

	/*
	public static Tween spriteAlphaFrom( Sprite target, float fromValue, float duration, float delay = 0.0f )
	{
		//Debug.Log ("> ALPHA FROM");
		float endValue = target..alpha;
		canvasGroup.alpha = fromValue;
		return DOTween.To(()=> canvasGroup.alpha, x=> canvasGroup.alpha = x, endValue, duration).SetDelay(delay).SetEase(Ease.OutCubic).SetAutoKill(false);
	}
	
public static Tween spriteAalphaTo( Sprite target, float toValue, float duration, float delay = 0.0f )
	{
		//Debug.Log ("ALPHA TO");
		/*
		UITweener theTween = TweenAlpha.Begin(target, duration,toValue);
		theTween.method = UITweener.Method.EaseOut;
		theTween.delay = delay;
		* /
		return DOTween.To(()=> canvasGroup.alpha, x=> canvasGroup.alpha = x, toValue, duration).SetDelay(delay).SetEase(Ease.OutCubic).SetAutoKill(false);
	}
	*/
	
	
	// ALPHA
	/*
 	public static Tween alphaFrom( float fromValue, float duration, float delay = 0.0f )
	{
		float endValue = initAlpha;
		canvasGroup.alpha = fromValue;
		return DOTween.To(()=> canvasGroup.alpha, x=> canvasGroup.alpha = x, endValue, duration).SetDelay(delay).SetEase(Ease.OutCubic);//.SetAutoKill(false);
	}
	public static Tween alphaTo( float toValue, float duration, float delay = 0.0f )
	{
		return DOTween.To(()=> canvasGroup.alpha, x=> canvasGroup.alpha = x, toValue, duration).SetDelay(delay).SetEase(Ease.OutCubic);//.SetAutoKill(false);
	}
	*/


}
