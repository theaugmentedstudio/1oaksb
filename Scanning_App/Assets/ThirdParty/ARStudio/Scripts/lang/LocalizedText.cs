using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{

	/** -------------------------------------- PROPERTIES ----------------------------------------*/

	// INSPECTOR
	public string Key; // The key to look at in the language pack
	Text textRef;
	string[] replaceKeys;	// if needed replace some values inside the string with this key-value system
	string[] replaceValues;

	/** -------------------------------------- MONO ----------------------------------------*/

	void Awake()
	{
		LanguageManager.LANGUAGE_LOADED += UpdateText;
		textRef = gameObject.GetComponent<Text> ();
		if (!textRef) Debug.LogError ("LocalizedText for object '"+gameObject.name+"' do not have a text component");
		if (LanguageManager.PackLoaded) UpdateText ();
	}
	void OnDestroy()
	{
		LanguageManager.LANGUAGE_LOADED -= UpdateText;
	}

	/** -------------------------------------- PUBLIC ----------------------------------------*/

	public void UpdateText()
	{
		UpdateText (replaceKeys, replaceValues);
	}

	public void UpdateText( string[] _replaceKeys, string[] _replaceValues )
	{
		string txt = LanguageManager.GetText (Key);
		if (_replaceKeys != null && _replaceValues != null && _replaceKeys.Length == _replaceValues.Length) 
		{
			replaceKeys = _replaceKeys;
			replaceValues = _replaceValues;
			for (int i = 0; i < replaceKeys.Length; i++) {
				txt = txt.Replace("{{"+_replaceKeys[i]+"}}", _replaceValues[i]);
			}
		}

		// set text
		if (textRef != null) textRef.text = txt;
	}
}
