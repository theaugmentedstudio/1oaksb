using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum Language
{
	EN
	,FR
	,NL
}

public static class LanguageManager
{
	/* -------------------------------------- EVENT/DELEGATE ---------------------------------------- */

	public delegate void languageAction();
	public static event languageAction LANGUAGE_LOADED;


	/* -------------------------------------- PROPERTIES ---------------------------------------- */

	public static bool PackLoaded = false;
	public static Language CurrentLanguage = Language.EN;
	private static LanguagePack loadedLanguagePack = null;
	public static string LanguagePackDirectory = "/Resources/lang/";
	
	static string buffer = "";

 	/* -------------------------------------- PUBLIC REGION ---------------------------------------- */
	 
	public static string GetText(string key)
	{
		if(PackLoaded)
		{
			buffer = loadedLanguagePack.GetString(key);
			buffer = buffer.Replace("#n", System.Environment.NewLine);
			return buffer;
		}
		else
		{
			return "No Pack loaded";
		}
	}
	
	public static void LoadLanguageFile(Language language)
	{
		loadedLanguagePack = LoadToPack(language);
	}


	/* -------------------------------------- PRIVATE REGION ---------------------------------------- */

	static LanguagePack LoadToPack(Language language)
	{
		CurrentLanguage = language;
		
		try
		{
			string loadName = System.Enum.GetName(typeof(Language), language);
			string filePath = LanguagePackDirectory + loadName;
			LanguagePack pk = new LanguagePack();
			
			// Read in the STRING contents of xmlFile (TextAsset)	
			TextAsset xmlFile;
			string t;
			
			if(LanguagePackDirectory.StartsWith("/Resources"))
			{	
				filePath = filePath.Substring(11);
				xmlFile = (TextAsset)Resources.Load(filePath, typeof(TextAsset));
				t = xmlFile.text;
				Debug.Log("LanguateManager.LoadToPack : " + t );
			}
			else
			{	
				StreamReader sr = new StreamReader(Application.dataPath + filePath + ".txt");
				t = sr.ReadToEnd();
				sr.Close();
			}
			
			using(StringReader f = new StringReader(t))
			{
				// JSON
				string errorMessage = pk.FillFromJSON(f.ReadToEnd());
				if(errorMessage != null)
				{
					Debug.LogError("Error while parsing language pack : "+errorMessage);
					return null;
				}

				// XML
				//...

				// TEXT
				/*
				string line;
				while((line = f.ReadLine()) != null)
				{
					try
					{
						pk.AddNewString((string)(line.Split(new string[] { " = " }, System.StringSplitOptions.None)[0]), line.Split(new string[] { " = " }, System.StringSplitOptions.None)[1]);
						//Debug.Log(line);
					} 
					catch 
					{
						if(line.Length > 0)
							LanguagePackDirectory = line;
					}
				}	
				*/
			}
			
			PackLoaded = true;
			if(LANGUAGE_LOADED != null) LANGUAGE_LOADED();
			return pk;
		}
		catch (System.Exception ex)
		{
			Debug.Log("No File Was Found! Make sure the language file you are trying to load exists! : "+ex.ToString());
			PackLoaded = false;
			return null;
		}
	}


	/*
	public static void SaveLanguageFile(Language language)
	{
		//Convert Enum into a String (Not recommended method but tough!)
		string saveName = System.Enum.GetName(typeof(Language), language);
		string filePath = Application.dataPath + LanguagePackDirectory + saveName + ".txt";
		StreamWriter f = new StreamWriter(filePath);
		
		if(CurrentLanguagePack==null)
			loadedLanguagePack = new LanguagePack();
		
		for(int i = -1; i  < CurrentLanguagePack.Keys.Count; i++)
		{
			if(i == -1)
				f.WriteLine(LanguagePackDirectory);
			else			
				f.WriteLine(CurrentLanguagePack.Keys[i] + " = " + CurrentLanguagePack.Strings[i]);
			
			f.WriteLine("\n");
		}
		f.Close();
		
		PackLoaded = true;
	}
	*/

}

[System.Serializable]
public class LanguagePack
{
	List<string> stringKeys = new List<string>();
	List<string> strings = new List<string>();

	public string FillFromJSON( string jsonString)
	{
		try 
		{
			// transform result in dictionnary
			Dictionary<string, object> dic = (Dictionary<string,object>) MiniJSON.Json.Deserialize( jsonString );

			foreach(KeyValuePair<string, object> entry in dic)
			{
				stringKeys.Add(entry.Key);
				strings.Add(entry.Value.ToString());
			}

			//if(glossary.ContainsKey("blablbla")) ....
		} 
		catch (System.Exception ex)
		{
			return ex.ToString();
		}
		
		return null; // no error
	}


	public string GetString(string key)
	{
		if(stringKeys.Contains(key))
		{
			return strings[stringKeys.IndexOf(key)];
		}
		else
		{
			//Debug.LogWarning("The Key: \""+key+"\" was not found...");
			//return "BAD KEY";
			return "??" + key + "??";
		}
	}
}
