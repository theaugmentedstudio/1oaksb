using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Console : Singleton<Console>
{

 	/* -------------------------------------- PROPERTIES ---------------------------------------- */
	 

	// inspector
	bool SHOW_CONSOLE = true;
	bool SHOW_LOG_BUTTON = true;
	bool SHOW_CLEAR_DATA_BUTTON = true;

	float buttonX = 150.0f;
	float buttonY = 5.0f;



	// data
	string VERSION = AppData.appVo.buildVersion;
	string devMailUrl = AppData.serviceVo.debugUrl + "devMail.php";
	string consoleText ="";
	bool initDone = false;
	Vector2 scrollViewVector = Vector2.zero;
	bool consoleOpen = false;


	/**
 	 * -------------------------------------- MONO BEHAVIOR ----------------------------------------
	 */
	protected override void Awake ()
	{
		base.Awake ();

		if(initDone) return;
		if(DebugVo.CLEAR_PLAYERPREFS_AT_START) PlayerPrefs.DeleteAll();
		
		// setup log callback
		Application.logMessageReceived += unityLogCallBack;
		resetConsoleLogs ();
		initDone = true;
	}

	void resetConsoleLogs()
	{
		consoleText = "Build : "+VERSION + "\n";
		consoleText += "DeviceModel : "+SystemInfo.deviceModel + "\n";
		consoleText += "DeviceType : "+SystemInfo.deviceType + "\n";
		consoleText += "DeviceName : "+SystemInfo.deviceName + "\n";
		consoleText += "SystemMemorySize : "+ (SystemInfo.systemMemorySize/1000) + "Gb \n";
		//consoleText = "OS : "+iPhoneSettings.systemVersion + "\n";
		consoleText += "> OS : " + SystemInfo.operatingSystem + "\n";
	}

	/*
	void OnEnable () {
		// add callback to unity log system
		Application.RegisterLogCallback(unityLogCallBack);
	}
	void OnDisable () {
		// Remove callback when object goes out of scope
		Application.RegisterLogCallback(null);
	}
	*/
	//void OnGUI () {
		// render console content and view
		//onRenderConsole ();
	//}

	/**
 	 * -------------------------------------- RENDER CONSOLE VIEW ----------------------------------------
	 */

	void onRenderConsole()
	{
		float newConsoleX = buttonX;
		if (SHOW_CONSOLE) { 
			
			GUI.depth = 1000; 

			// CONSOLE SWITCH
			if (GUI.Button(new Rect (newConsoleX, buttonY,100,25.0f), "Console on/off")){
				consoleOpen = !consoleOpen;
			}

			// CONSOLE CLEAR
			if(consoleOpen)
			{
				float windowWidth = Screen.width;
				float posY = buttonY + 20;
				GUI.BeginGroup (new Rect(0, posY, Screen.width, Screen.height-posY));
				scrollViewVector = GUI.BeginScrollView (new Rect (0, 0.0f, windowWidth, Screen.height), scrollViewVector, new Rect (0.0f, 0.0f, 400.0f, 2000.0f));
				GUI.TextArea (new Rect (0, 0.0f, windowWidth - 20.0f, 2000.0f), consoleText);
				GUI.EndScrollView();
				GUI.EndGroup ();

				newConsoleX = newConsoleX+100;
				if (GUI.Button(new Rect (newConsoleX ,buttonY,100,20.0f),"Clear")){
					resetConsoleLogs();
				}
			}

		}
		
		if (SHOW_LOG_BUTTON) 
		{
			newConsoleX = newConsoleX + 100;
			if (GUI.Button(new Rect (newConsoleX , buttonY,100,25.0f), "Send log")){
				sendDevMail();
			}
		}
		
		if (SHOW_CLEAR_DATA_BUTTON) 
		{
			newConsoleX = newConsoleX + 100;
			if (GUI.Button(new Rect (newConsoleX , buttonY,100,25.0f), "Clear user data")){
				PlayerPrefs.DeleteAll();
			}
		}

		// THIS IS SUCH A BAD HACK... but not able to understand why it crashes without this on nexus 5 (AR view)
		/*
		if (use_bad_android_hack) 
		{
			GUI.color = new Color(1.0f,1.0f,1.0f,.0f);
			GUI.Button(new Rect(250,0,50,30),"");
			GUI.color = new Color(1.0f,1.0f,1.0f,1.0f);
		}
		*/

	}


	/**
 	 * -------------------------------------- LOG CALLBACK ----------------------------------------
	 */

	/**
 	 * Unity default log callback (to get errors, warns and logs in console and dev mail)
	 */
	void unityLogCallBack ( string logString, string stackTrace, LogType type) 
	{
		//init ();
		print ("------->LOG_MESSAGE RECEIVED");
		if (type == LogType.Log) log (logString);
		else if (type == LogType.Error) logError (""+ logString +" : " + stackTrace);
		else if (type == LogType.Exception) logError (""+ logString +" : " + stackTrace);
		else if (type == LogType.Warning) logWarning (""+ logString +" : " + stackTrace);
	}
	void log(string message)
	{
		consoleText = consoleText + "\n" + "["+getLogTime()+"][LOG] "+message;
		//Debug.Log(message);
	}
	void logWarning(string message)
	{
		consoleText = consoleText + "\n" + "["+getLogTime()+"][WARNING] "+message;
		//Debug.Log(message);
	}
	void logError(string message)
	{
		consoleText = consoleText + "\n" + "["+getLogTime()+"][ERROR] "+message;
		//Debug.LogError(message);

		// send mail
		if (instance && !Application.isEditor) instance.sendDevMail ();
		//if (instance) instance.sendDevMail ();

		// notify on flurry
		//logFlurryErrorEvent ( "[ERROR] "+message );

		// notify ga
		GoogleAnalyticsV3.instance.LogException (message, false);
	}
	string getLogTime()
	{
		return System.DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
	}

	/*
	public static void LogJson (string json)
	{

		string text = string.Empty;
		if (json != null)
		{
			text = JsonFormatter.prettyPrint (json);
		}
		try
		{
			Debug.Log (text);
		}
		catch (Exception)
		{
			Console.WriteLine (text);
		}
	}
	*/


	/**
 	 * -------------------------------------- SEND DEV MAIL ----------------------------------------
	 */

	public void SendDevMail(string message)
	{
		logError (">>> send error to webservice : " + message);
		if (instance) instance.sendDevMail ();
	}
	bool isSending = false;
	int numMailSent = 0;
	void sendDevMail () 
	{
		if( isSending || DebugVo.DO_NOT_SEND_DEVMAIL ) return;
		numMailSent ++;
		if(numMailSent > 3) return; // we are in a loop... avoid sending 1000x the same message

		// prepare mail
		isSending = true;
		Debug.Log ("Console.sendDevMail : " +devMailUrl);
		WWWForm form = new WWWForm();
		form.AddField("msg",""+consoleText);
		//form.AddField("email",""+AppData.userVo.email);

		//
		WWW www = new WWW(devMailUrl, form);
		StartCoroutine(WaitForRequest(www));
	}
	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		// check for errors
		Debug.Log  ("Console.sendDevMail result : ");
		if (www.error == null)
		{
			Debug.Log ("==> Dev Mail Result: " + www.text);
		} else {
			Debug.Log ("==> Dev Mail Error: "+ www.error);
		}  
		isSending = false;  
	}


	/**
 	 * -------------------------------------- LOG FLURRY EVENT ----------------------------------------
	 */
	/*
	private static void logFlurryErrorEvent(string msg)
	{
		if(instance && instance.numMailSent > 3) return; // we are in a loop... avoid sending 1000x the same message

		var dict = new Dictionary<string,string>();
		dict.Add( "msg", ""+ msg );
		dict.Add( "email", ""+Infos.user_email );
		         
		#if UNITY_ANDROID
		FlurryAndroid.logEvent( "errorEvent", dict, false  );
		#elif UNITY_IPHONE
		FlurryAnalytics.logEventWithParameters( "errorEvent", dict, false );
		#endif
	}
	*/

}