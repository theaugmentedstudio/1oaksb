﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayVersion : MonoBehaviour 
{
	private Text textfield;
	void Start()
	{
		textfield = GetComponent<Text> ();
		if( !textfield )
		{
			Debug.Log("DisplayVersion needs a Text component!");
			enabled = false;
			return;
		}
		textfield.text = "v " + AppData.appVo.buildVersion;
	}
}