﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class LogTime 
{
	static Stopwatch stopwatch = new Stopwatch ();
	static string _logTag;

	public static void Start ( string logTag )
	{
		if (stopwatch.IsRunning) {
			UnityEngine.Debug.LogWarning("Stopwatch '"+_logTag+"' already running");
			return;
		}
		_logTag = logTag;
		stopwatch.Start ();
	}

	public static void StopAndPrint ()
	{
		stopwatch.Stop();
		UnityEngine.Debug.Log (":::::::::::::::::::::: TIME ELAPSED FOR '"+_logTag+"' : "+stopwatch.Elapsed.TotalMilliseconds+"ms ::::::::::::::::::::::");
		stopwatch.Reset ();
	}
}
