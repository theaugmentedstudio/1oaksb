﻿using UnityEngine;
using System.Collections;

public class RaycastDebug : MonoBehaviour {

	public Camera camToCheck;

	// Use this for initialization
	void Start () {
		Debug.LogWarning ("PLEASE REMOVE RAYCAST DEBUG FROM HIERARCHY, this is for testing only");
	}
	
	void Update () {
		if (!camToCheck) return;
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = camToCheck.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				Debug.Log ("Name = " + hit.collider.name);
				Debug.Log ("Tag = " + hit.collider.tag);
				Debug.Log ("Hit Point = " + hit.point);
				Debug.Log ("Object position = " + hit.collider.gameObject.transform.position);
				Debug.Log ("--------------");
			}
		}
	}
}
