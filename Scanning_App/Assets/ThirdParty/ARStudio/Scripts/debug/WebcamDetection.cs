using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class WebcamDetection : MonoBehaviour {
 

	/**
 	 * -------------------------------------- INIT ----------------------------------------
	 */

	public void Start()
	{
		foreach (WebCamDevice wcd in WebCamTexture.devices) {
			Debug.Log("<color=yellow>WebCam Device: </color>" + wcd.name);
		}
	}
}