﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class KeyboardMove : MonoBehaviour, ISelectHandler,IDeselectHandler
{
	public RectTransform[] objectsToMove;
	Vector2[] initPos;

	void Start()
	{
		//saveInitPos (); EDIT : this breaks animations
	}
	void saveInitPos()
	{
		int index = 0;
		initPos = new Vector2[objectsToMove.Length];
		foreach (var item in objectsToMove) {
			initPos[index] = item.anchoredPosition;
			index ++;
		}
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		if (initPos == null) saveInitPos ();
		int index = 0;
		foreach (var item in objectsToMove) {
			moveTo (item, new Vector2 (initPos[index].x, initPos[index].y +200), 0.5f);	
			index ++;
		}
	}

	#endregion

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{
		if (initPos == null) saveInitPos ();
		int index = 0;
		foreach (var item in objectsToMove) {
			moveTo (item, new Vector2 (initPos[index].x, initPos[index].y), 0.5f);	
			index ++;
		}
	}

	#endregion

	Tween moveTo( RectTransform target, Vector2 newPos, float duration, float delay = 0.0f )
	{
		//Debug.Log ("> MOVE BY");
		//Vector2 endValue = target.anchoredPosition;
		//target.anchoredPosition = new Vector2 (endValue.x + byValues.x, endValue.y + byValues.y);
		return DOTween.To(() => target.anchoredPosition, x=> target.anchoredPosition = x, newPos, duration).SetDelay(delay).SetEase(Ease.OutCubic).SetAutoKill(false);
	}

}
