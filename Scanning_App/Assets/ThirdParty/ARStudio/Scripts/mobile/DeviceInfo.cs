﻿using UnityEngine;
using System.Collections;

public class DeviceInfo {


	public enum DeviceType
	{
		None,
		iPad,
		iPhone,
	}

	// status
	public static bool isHighRes = Screen.height>768.0f;
	public static DeviceType deviceType = Screen.height>640.0f ? DeviceType.iPad : DeviceType.iPhone;

}
