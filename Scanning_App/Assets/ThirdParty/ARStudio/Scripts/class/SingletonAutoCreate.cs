﻿using UnityEngine;
public class SingletonAutoCreate<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T instance;
	
	/**
     Returns the instance of this singleton.
   	*/
	public static T Instance
	{
		get
		{
			if(instance == null)
			{
				instance = (T) FindObjectOfType(typeof(T));
				
				if (instance == null)
				{
					Debug.Log("An instance of " + typeof(T) + 
					               " is needed in the scene, we create it.");
					GameObject g = new GameObject("" + typeof(T));
					instance = g.AddComponent(typeof(T)) as T;
				}
			}
			
			return instance;
		}
	}

	protected virtual void Awake()
	{
		if(instance == null)
		{
			instance = this as T;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			DestroyImmediate(gameObject);
		}
	}

}