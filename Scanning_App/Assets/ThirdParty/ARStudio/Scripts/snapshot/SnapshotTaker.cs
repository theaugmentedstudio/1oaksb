using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


public class SnapshotTaker : MonoBehaviour
{
	/**
 	 * -------------------------------------- PROPERTIES ----------------------------------------
	 */

	// event type
	public delegate void VoidHandler();

	// events
	public static event VoidHandler TEXTURE_READY;
	public static event VoidHandler FILE_READY;
	public static event VoidHandler TEXTURE_LOAD_FAIL;
	
	// public static val
	public static Texture2D SnapShotTexture;
	public static Texture2D SnapShotTextureCopy
	{
		get{
			if(!SnapShotTexture) return null;
			Color32[] pix = SnapShotTexture.GetPixels32();
			//System.Array.Reverse(pix);
			Texture2D destTex = new Texture2D(SnapShotTexture.width, SnapShotTexture.height);
			destTex.SetPixels32(pix);
			destTex.Apply();
			return destTex;
		}
	}



	// props
	static string screenshotFilename = "tempScreenshot.png";
	static string folderPath;
	bool snapshot = false;
	GameObject[] itemsToHideDuringSnapshot;

	
	/**
 	 * -------------------------------------- MONO BEHAVIOR SINGLETON PATTERN ----------------------------------------
	 */

	private SnapshotTaker(){} // private constructor
	private static SnapshotTaker _instance; 
	public static SnapshotTaker instance
	{
		get
		{
			if(!_instance){
				GameObject g = new GameObject("_SnapshotTaker");
				DontDestroyOnLoad(g);
				_instance = g.AddComponent(typeof(SnapshotTaker)) as SnapshotTaker;
				_instance.init ();
			}
			return _instance;
		}
	}

	/**
 	 * -------------------------------------- INIT ----------------------------------------
	 */

	void init()
	{
		log("SnapshotTaker.init");
		folderPath = Application.persistentDataPath;
		log(" => folderPath : "+folderPath);
	}


	/**
 	 * -------------------------------------- RETRIEVE SCREENSHOT PATH ----------------------------------------
	 */
	 
	public string GetSnapshotFilePath()
	{
		string pathToImage = folderPath + "/" + screenshotFilename;
		return pathToImage;
		/*
		if( System.IO.File.Exists( pathToImage ) ) return pathToImage;
		else return null;
		*/
	}


	/**
 	 * -------------------------------------- CAPTURE SCREENSHOT IN TEXTURE 2D ----------------------------------------
 	 * This method use a simple texture read pixel to capture screenshot
	 */
	private Rect nullRect = new Rect(-1,-1,-1,-1);
	 
	public void CaptureScreenshot()
	{
		StartCoroutine(captureScreenshot( null , nullRect));
	}
	public void CaptureScreenshot( GameObject[] itemsToHide )
	{
		StartCoroutine(captureScreenshot( itemsToHide, nullRect ));
	}
	public void CaptureScreenshot( GameObject[] itemsToHide, Rect sourceRect )
	{
		StartCoroutine(captureScreenshot( itemsToHide, sourceRect ));
	}
	IEnumerator captureScreenshot( GameObject[] itemsToHide, Rect sourceRect )
	{
		itemsToHideDuringSnapshot = itemsToHide;
		hideItems();
		yield return new WaitForEndOfFrame();

		if (sourceRect == nullRect) sourceRect = new Rect (0, 0, Screen.width, Screen.height); 
		SnapShotTexture = new Texture2D((int)sourceRect.width, (int)sourceRect.height, TextureFormat.RGB24, false );
		SnapShotTexture.ReadPixels( sourceRect, 0, 0 );
		SnapShotTexture.Apply();
		
		showItems();
		if(TEXTURE_READY != null) TEXTURE_READY();
	}
	
	
	/**
 	 * -------------------------------------- HIDE / SHOW ITEMS not wanted on screenshot  ----------------------------------------
	 */
	
	void hideItems()
	{
		// hide item before capture
		if(itemsToHideDuringSnapshot != null){
			for (int i = 0; i < itemsToHideDuringSnapshot.Length; i++) {
				itemsToHideDuringSnapshot[i].SetActive(false);
			}
		}
	}
	void showItems()
	{
		// show ui camera
		//UICamera.currentCamera.enabled = true;
		if(itemsToHideDuringSnapshot != null){
			for (int i = 0; i < itemsToHideDuringSnapshot.Length; i++) {
				itemsToHideDuringSnapshot[i].SetActive(true);
			}
		}
	}
	
	 
	 
	/**
 	 * -------------------------------------- CAPTURE SCREENSHOT with default unity screenshot to file ----------------------------------------
 	 * Capture and save are done via default capturescreenshot method, file is created at same time
	 */
	 
	 
	 
	public void CaptureAndSaveScreenshot()
	{
		CaptureAndSaveScreenshot(null);
	}
	public void CaptureAndSaveScreenshot(GameObject[] itemsToHide)
	{
		Debug.Log("SnapshotTaker.CaptureAndSaveScreenshot : "+itemsToHide.Length+" items to hide");
		itemsToHideDuringSnapshot = itemsToHide;
		deleteExistingScreenshot();
		StartCoroutine(captureAndSaveScreenshot());
	}
	IEnumerator captureAndSaveScreenshot()
	{
		log("SnapshotTaker.captureScreenshot");
		
		// Default behavior is to hide NGUI Camera..
		//UICamera.currentCamera.enabled = false;
		hideItems();
		
		// capture
		if(Application.isEditor) Application.CaptureScreenshot(folderPath +"/"+ screenshotFilename);
		else Application.CaptureScreenshot(screenshotFilename);
		
		Invoke("showItems", 1.0f);
		
		// wait until screenshot is created
		var pathToImage = folderPath + "/" + screenshotFilename;
		while( !System.IO.File.Exists( pathToImage ) )
		{
			yield return null;
		}
		
		// screenshot created
		snapshot = true;
		
		// file created
		if( FILE_READY != null ) FILE_READY();
		
		StartCoroutine(importLocalImage(GetSnapshotFilePath()));	
	}
	
	
	
	/**
 	 * -------------------------------------- CAPTURE SPECIFIC CAMERA CONTENT --------------------------------------
 	 * Capture content of a specific camera
	 */
	
	
	
	public void CaptureCameraScreenshot (Camera camera, int width, int height, Vector3? position = null, Quaternion? rotation = null )
	{
		Debug.Log("SnapshotTaker.CaptureCameraScreenshot");
		deleteExistingScreenshot();
		StartCoroutine(captureCameraScreenshot(camera, width, height, position, rotation));
	}
	IEnumerator captureCameraScreenshot (Camera camera, int width, int height, Vector3? position = null, Quaternion? rotation = null )
	{
		yield return new WaitForEndOfFrame();
		
		SnapShotTexture = new Texture2D (Screen.width, Screen.height);
		if (rotation != null)
		{
			camera.transform.rotation = rotation.Value;
		}
		
		if (position != null)
		{
			camera.transform.position = position.Value;
		}
		
		camera.Render();
		RenderTexture.active = camera.targetTexture;
		SnapShotTexture.ReadPixels(new Rect(0,0,Screen.width,height),0,0);
		
		SnapShotTexture.Apply();
		RenderTexture.active = null;
		
		// notify texture ready
		if(TEXTURE_READY != null) TEXTURE_READY();
	}
	
	
	
	/**
 	 * -------------------------------------- CAPTURE ALL CAMERA CONTENT --------------------------------------
 	 *  EDIT : this seems to break on iOS (white texture issue)
	 */
	public void CaptureAllCamerasScreenshot()
	{
		Debug.Log("SnapshotTaker.Capture-ALL-CamerasScreenshot");
		StartCoroutine(captureAllCamerasScreenshot(null));
	}
	public void CaptureAllCamerasScreenshot(GameObject[] itemsToHide)
	{
		Debug.Log("SnapshotTaker.Capture-ALL-CamerasScreenshot");
		StartCoroutine(captureAllCamerasScreenshot(itemsToHide));
	}
	IEnumerator captureAllCamerasScreenshot(GameObject[] itemsToHide)
	{
		// save items to hide
		itemsToHideDuringSnapshot = itemsToHide;
		
		// hide items we don't want on camera
		hideItems();
		
		// wait to be sure items are correctly hidden
		yield return new WaitForSeconds(0.1f);
		
		// get camera list
		List<Camera> cameras = new List<Camera>(Camera.allCameras);
		// sort cameras using Depthcon
		//cameras.Sort(new CameraComparer());
		
		// setup render texture
		int a_Width = Screen.width;
		int a_Height = Screen.height;
		RenderTexture renderTexture =  new RenderTexture(a_Width, a_Height, 50, RenderTextureFormat.ARGB32); //RenderTexture.GetTemporary(a_Width, a_Height, 24);
		RenderTexture.active = renderTexture;  
		
		// render all cameras
		foreach (Camera camera in cameras)
		{
			Debug.Log("Camera.name = "+camera.name + " and is enabled = "+ camera.enabled+ " & depth = " +camera.depth + " & clearflag = "+camera.clearFlags);
			if (camera.enabled)
			{
				float fov = camera.fieldOfView;
				camera.targetTexture = renderTexture;
				camera.Render();
				camera.targetTexture = null;
				camera.fieldOfView = fov;
			}
		}
		
		// set it on snapshottexture
		if(SnapShotTexture) Destroy(SnapShotTexture);
		SnapShotTexture = new Texture2D (a_Width, a_Height);
		SnapShotTexture.ReadPixels(new Rect(0,0,a_Width,a_Height),0,0);
		SnapShotTexture.Apply();
		
		// desactive and kill rendertexture
		RenderTexture.active = null;
		Destroy(renderTexture);
		//RenderTexture.ReleaseTemporary(renderTexture);
	
		// reset items
		showItems();

		// notify texture ready
		if(TEXTURE_READY != null) TEXTURE_READY();
	}
	
	
	
	/**
 	 * -------------------------------------- SAVE CURRENT SCREENSHOT TO FILE --------------------------------------
 	 */
	 
	public void SaveScreenshotToFile ()
	{
		SaveScreenshotToFile( 0, 0 );
	}
	public void SaveScreenshotToFile ( int width, int height )
	{
		Debug.Log("SnapshotTaker.SaveScreenshotToFile");
		
		// check if we need to rescale
		if( (width != 0 && SnapShotTexture.width != width) || (height != 0 && SnapShotTexture.height != height))
		{
			Debug.Log("SnapshotTaker.scaleScreenshot to : "+width+"x"+height);
			SnapShotTexture = ScaleTexture(SnapShotTexture, width, height);
		}
		
		byte[] imageBytes = SnapShotTexture.EncodeToPNG ();
		string filePath = folderPath + "/" + screenshotFilename;
		FileStream fs = File.Create(filePath);
		fs.Write(imageBytes, 0, imageBytes.Length);
		fs.Close();
		
		// wait until screenshot is created
		/*
		var pathToImage = folderPath + "/" + screenshotFilename;
		while( !System.IO.File.Exists( pathToImage ) )
		{
			yield return null;
		}
		*/
		
		// screenshot created
		snapshot = true;
		if( FILE_READY != null ) FILE_READY();
	}
	
	
	
	/**
 	 * -------------------------------------- LOAD SCREENSHOT INTO TEXTURE --------------------------------------
 	 * load existing file localy and load it into a texture 2D
 	 */
 	 
	IEnumerator importLocalImage(string imagePath)
	{
		if(!snapshot || imagePath == null) {
			logError("SnapshotTaker.importLocalImage : Texture can't be created as there is no snapshot available at : "+imagePath);
			TEXTURE_LOAD_FAIL();
		}
		
		string url = "file://" + imagePath;
		log("SnapshotTaker.importLocalImage : "+url);
		
		// Create a texture in DXT1 format
		SnapShotTexture = new Texture2D(1024, 1024, TextureFormat.RGB24, false);
		// Start a download of the given URL
		WWW www = new WWW(url);
		
		//previewTexture.renderer.material.mainTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
		
		// wait until the download is done
		yield return www;
		// assign the downloaded image to the main texture of the object
		www.LoadImageIntoTexture(SnapShotTexture);
		
		log(" --> image imported");
		TEXTURE_READY ();
	}
	
	
	/**
 	 * -------------------------------------- HELPERS --------------------------------------
 	 */
	
	/**
 	 *	SCALE A TEXTURE
	 */
	Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) 
	{
		Texture2D result = new Texture2D(targetWidth,targetHeight,source.format,true);
		Color[] rpixels=result.GetPixels(0);
		float incX=((float)1/source.width)*((float)source.width/targetWidth);
		float incY=((float)1/source.height)*((float)source.height/targetHeight);
		for(int px=0; px<rpixels.Length; px++) {
			rpixels[px] = source.GetPixelBilinear(incX*((float)px%targetWidth),
			                                      incY*((float)Mathf.Floor(px/targetWidth)));
		}
		result.SetPixels(rpixels,0);
		result.Apply();
		return result;
	}
	
	/**
 	 *	DELETE A SCREENSHOT
	 */
	void deleteExistingScreenshot()
	{
		deleteExistingScreenshot (screenshotFilename);
	}
	void deleteExistingScreenshot( string fileName )
	{
		snapshot = false;
		var pathToImage = folderPath + "/" + fileName;
		log("SnapshotTaker.deleteExistingScreenshot : " + pathToImage);
		if( System.IO.File.Exists( pathToImage ) )
		{
			log(" -> File exists, delete it");
			System.IO.File.Delete(pathToImage);
		}
	}
	
	

	/**
 	 * -------------------------------------- LOGS BRIDGE ----------------------------------------
	 */

	static void log(string message)
	{
		Debug.Log(message);
	}
	
	static void logError(string message)
	{
		Debug.LogError(message);
	}

}
