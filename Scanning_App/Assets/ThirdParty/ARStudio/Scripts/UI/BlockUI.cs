﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BlockUI {

	static EventSystem es;

	// Use this for initialization
	public static void Block ( bool flag )
	{
		if (!es) es = EventSystem.current;
		es.enabled = !flag;
	}

	public static bool isBlocked
	{
		get{ return !es.enabled;}
	}

}
