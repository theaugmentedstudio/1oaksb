﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.EventSystems;

// http://answers.unity3d.com/questions/884762/ui-inputfield-caret-position-wrong-when-midline-al.html

public class InputCaretPosFix : MonoBehaviour, ISelectHandler
{
	public float neededPivotRuntime = 0.5f;
	RectTransform caret;

	public void Awake ()
	{}

	/*
	public void OnSelect (BaseEventData eventData)
	{
		StartCoroutine( updateCaret () );
	}
	

	IEnumerator updateCaret()
	{
		yield return new WaitForSeconds (0.2f);
		caret = gameObject.transform.Find("InputField Input Caret").GetComponent<RectTransform>();
		caret.pivot = new Vector2(caret.pivot.x, neededPivotRuntime);
	}
	*/

	public void OnSelect(BaseEventData eventData) 
	{
		InputField ipFld = gameObject.GetComponent<InputField>();
		if(ipFld != null) {
			RectTransform textTransform = (RectTransform)ipFld.transform.FindChild("Text").GetComponent<Text>().transform;
			RectTransform caretTransform = (RectTransform)transform.Find(gameObject.name+" Input Caret");
			if(caretTransform != null && textTransform != null) {
				//caretTransform.localPosition = textTransform.localPosition;
				// antho fix
				caretTransform.pivot = new Vector2(caretTransform.pivot.x, neededPivotRuntime);
				caretTransform.gameObject.SetActive(false); // TODO : reset caret later
			}
		}

		/* FIX FROM  : http://forum.unity3d.com/threads/change-inputfield-caret.269311/
		InputField ipFld = gameObject.GetComponent<InputField>();
		if(ipFld != null) {
			RectTransform textTransform = (RectTransform)ipFld.text.transform;
			RectTransform caretTransform = (RectTransform)transform.Find(gameObject.name+" Input Caret");
			if(caretTransform != null && textTransform != null) {
				caretTransform.localPosition = textTransform.localPosition;
			}
		}
		*/
	}

}
