﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonTapEffect : MonoBehaviour , IPointerDownHandler, IPointerUpHandler
{
	// inspector
	public float scaleOnTap = 1.2f;
	public float scaleDuration = 0.3f;

	// props
	Vector3 initScale;

	/* -------------------------------------- MONO ---------------------------------------- */

	// Use this for initialization
	void Awake () {
		initScale = transform.localScale;
	}

	// enable
	void OnEnable()
	{
		gameObject.transform.localScale = initScale;
	}


	/* -------------------------------------- METHODS ---------------------------------------- */

	public void OnPointerDown(PointerEventData eventData)
	{
		DOTween.To(()=> transform.localScale, x=> transform.localScale = x, Vector3.one*scaleOnTap, scaleDuration).SetEase(Ease.OutBack);
	}
	
	public void OnPointerUp(PointerEventData eventData)
	{
		DOTween.To(()=> transform.localScale, x=> transform.localScale = x, initScale, 0.3f).SetEase(Ease.OutCubic);
	}

}
