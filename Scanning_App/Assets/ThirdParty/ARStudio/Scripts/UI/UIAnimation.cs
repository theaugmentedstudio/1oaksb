﻿using UnityEngine;
using System.Collections;
//using DG.Tweening;
//using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

/**
*/
[RequireComponent(typeof(RectTransform))]
public abstract class UIAnimation : MonoBehaviour {

	/**
 	 * -------------------------------------- PROPERTIES ----------------------------------------
	 */

	protected CanvasGroup canvasGroup; 
	protected RectTransform rectTransform;

	// SHOW 
	public float showDelay = 0;	// delay before showing 
	public float showDuration = 0.7f; // show duration
	public float showAlphaFrom = 1;
	public Vector2 showMoveBy = Vector2.zero;
	public Vector3 showScaleFrom = Vector3.one;
	protected Ease showEase = Ease.OutCubic;

	// HIDE
	public float hideDelay = 0; // hide
	public float hideDuration = 0.7f; // hide
	public float hideAlphaTo = 1;
	public Vector2 hideMoveAdd = Vector2.zero;
	public Vector3 hideScaleTo = Vector3.one;
	protected Ease hideEase = Ease.OutCubic;

	// PRIVATE
	protected bool isShowing = false;
	protected bool isHiding = false;
	protected float initAlpha ; // inital alpha of component
	protected Vector3 initPos ; // inital pos of component
	protected Vector3 initScale ; // inital pos of component

	// all items that will be used in show animation
	List<Tween> showTweenList = new List<Tween> ();
	List<Tween> hideTweenList = new List<Tween> ();
	
	//protected Tween alphaTween;



	/**
 	 * -------------------------------------- ON ENABLE ----------------------------------------
	 */
	
	void OnEnable()
	{
		// OnEnableImpl (); // done in the show so it's always done afer init

		// on enable, show 
		if(gameObject.activeSelf && gameObject.activeInHierarchy) StartCoroutine(show ());
	}
	// mandatory onEnable
	protected abstract void OnEnableImpl ();

	// not mandatory onLayoutComplete (done after first frame)
	protected virtual void OnLayoutCompleteImpl (){}


	/**
 	 * -------------------------------------- ON DISABLE ----------------------------------------
	 */
	
	void OnDisable()
	{
		OnDisableImpl ();
		// on enable, show 
		killShowTweens ();
		killHideTweens ();
	}
	// mandatory onEnable
	protected abstract void OnDisableImpl ();


	/**
 	 * -------------------------------------- GETTER/SETTER ----------------------------------------
	 */

	public bool IsShowing
	{
		get{return isShowing;}
	}
	public bool IsHiding
	{
		get{return isHiding;}
	}



	/**
 	 * -------------------------------------- INITIALIZE ----------------------------------------
	 */

	// mandatory init
	void init()
	{
		Debug.Log ("INIT (" + gameObject.name + ")");

		// get rect transform
		rectTransform = gameObject.GetComponent<RectTransform>(); // retrieve canvas group to manage alpha

		// save current pos and scale
		initPos = rectTransform.anchoredPosition;
		initScale = gameObject.transform.localScale;

		// to allow fading, we need a canvas group. If we do not have one, add one (TODO EDIT : maybe we could use the crossfade property of image later..)
		canvasGroup = gameObject.GetComponent<CanvasGroup>(); 
		if (!canvasGroup) canvasGroup = gameObject.AddComponent<CanvasGroup>(); 
		initAlpha = canvasGroup.alpha;

		// sub class init
		initImpl ();
	}
	// mandatory init
	protected virtual void initImpl (){}


	/**
 	 * -------------------------------------- SET VIEW ACTIVE OR NOT ----------------------------------------
	 */
	
	/**
	*  shortcut to set view active
	*/
	public void SetActive( bool flag)
	{
		gameObject.SetActive(flag);
	}


	/**
 	 * -------------------------------------- SHOW ----------------------------------------
	 */

	// mandatory show
	IEnumerator show()
	{
		// wait for anchors to setup
		//yield return new WaitForEndOfFrame();
		if (rectTransform == null) init (); // init must be done once the anchor are setup, otherwise we don't get correct anchored position

		// enable implementation
		OnEnableImpl ();

		// hide until end of frame (allow layout to be updated)
		canvasGroup.alpha = 0;

		// wait end of frame to allow layout to be generated
		yield return new WaitForEndOfFrame ();

		// implementation for after layout calculation
		OnLayoutCompleteImpl ();

		if(isShowing) yield break; // do not show twice
		Debug.Log ("SHOW (" + gameObject.name + ")");

		// create show tween list
		showTweenList = new List<Tween> ();

		// add show anims
		if(showAlphaFrom != 1) addShowAnim(alphaFrom (showAlphaFrom, showDuration, showDelay ));
		else canvasGroup.alpha = 1;
		if(showMoveBy != Vector2.zero) addShowAnim(moveBy (showMoveBy, showDuration, showDelay ) );
		//else rectTransform.anchoredPosition = initPos;
		if(showScaleFrom != Vector3.one) addShowAnim(scaleFrom (showScaleFrom, showDuration, showDelay ) );
		//else transform.localScale = initScale;

		// add other other tween
		addSubElementsShowTweens ();

		// reset canvasgroup interaction if it was removed from hide
		canvasGroup.interactable = true; 

		// on show complete callback to first tween (generally alpha)
		if (showTweenList.Count > 0) 
		{
			showTweenList [0].OnComplete (onShowFinished);
			canvasGroup.interactable = false;//BlockUI.Block(true); // block global ui while animating
			isShowing = true;
			
			// play animations
			foreach (Tween tween in showTweenList) 
			{
				try{
					tween.Restart ();
					tween.Play ();
				}
				catch(UnityException e)
				{
					Debug.LogWarning("Tween ERROR : "+e.ToString());
				}
			}
		}
		else 
		{
			onShowFinished();
		}
	}
	protected virtual void addSubElementsShowTweens (){}

	void onShowFinished()
	{
		isShowing = false;
		killShowTweens ();
		//BlockUI.Block (false);
		canvasGroup.interactable = true;
		onShowFinishedImpl ();
	}
	protected virtual void onShowFinishedImpl (){}

	// show may not occur is component is still visible (or is hiding)
	// force show reset hide tweens and force reenabling component
	public void ForceReShow()
	{
		killHideTweens ();
		SetActive(false);
		SetActive (true);
	}

	public void Show()
	{
		SetActive (true);
	}


	/**
 	 * -------------------------------------- SHOW /HIDE  shortcuts ----------------------------------------
	 */

	public void ShowFromLeft()
	{
		if (showDuration <= 0) showDuration = 1.0f;
		showMoveBy = new Vector2 (-screenWidth, 0);
		showScaleFrom = Vector3.one;
		Show ();
	}
	public void ShowFromRight()
	{
		if (showDuration <= 0) showDuration = 1.0f;
		showMoveBy = new Vector2 (screenWidth, 0);
		showScaleFrom = Vector3.one;
		Show ();
	}
	public void ShowFromTop()
	{
		if (showDuration <= 0) showDuration = 1.0f;
		showMoveBy = new Vector2 (0, screenHeight);
		showScaleFrom = Vector3.one;
		Show ();
	}
	public void ShowFromBottom()
	{
		if (showDuration <= 0) showDuration = 1.0f;
		showMoveBy = new Vector2 (0, -screenHeight);
		showScaleFrom = Vector3.one;
		Show ();
	}


	public void HideToLeft()
	{
		if (hideDuration <= 0) hideDuration = 1.0f;
		hideMoveAdd = new Vector2 (-screenWidth,0);
		hideScaleTo = Vector3.one;
		hide ();
	}
	public void HideToRight()
	{
		if (hideDuration <= 0) hideDuration = 1.0f;
		hideMoveAdd = new Vector2 (screenWidth,0);
		hideScaleTo = Vector3.one;
		hide ();
	}
	public void HideToTop()
	{
		if (hideDuration <= 0) hideDuration = 1.0f;
		hideMoveAdd = new Vector2 (0,screenHeight);
		hideScaleTo = Vector3.one;
		hide ();
	}
	public void HideToBottom()
	{
		if (hideDuration <= 0) hideDuration = 1.0f;
		hideMoveAdd = new Vector2 (0,-screenHeight);
		hideScaleTo = Vector3.one;
		hide ();
	}

	// ref width
	private float screenWidth
	{
		get
		{
			return 1920;
		}
	}

	// ref height
	private float screenHeight
	{
		get
		{
			return 1080;
		}
	}


	/**
 	 * -------------------------------------- HIDE ----------------------------------------
	 */

	public virtual void hide()
	{
		// do nothing if not visible..
		if (!gameObject.activeSelf)
			return;

		// abord show
		if (isShowing) killShowTweens ();

		// do not hide but directly disable
		if (!gameObject.activeInHierarchy || canvasGroup == null) {
			onHideFinished ();
			return;
		}

		// do not hide twice
		if ( isHiding ) return; 
		
		Debug.Log ("HIDE (" + gameObject.name + ")");



		// create show tween list
		hideTweenList = new List<Tween> ();
		
		// add show anims
		if(hideAlphaTo != 1) addHideAnim(alphaTo (hideAlphaTo, hideDuration, hideDelay ));
		if(hideMoveAdd != Vector2.zero) addHideAnim(moveAdd (hideMoveAdd, hideDuration, hideDelay ) );
		if(hideScaleTo != Vector3.one) addHideAnim(scaleTo (hideScaleTo, hideDuration, hideDelay ) );

		// add other other tween
		addSubElementsHideTweens ();
		
		// on show complete callback to first tween (generally alpha)
		if (hideTweenList.Count > 0) {
			hideTweenList [0].OnComplete (onHideFinished);
			canvasGroup.interactable = false;// block ui while animating
			isHiding = true;
			
			// play animations
			foreach (Tween tween in hideTweenList) {
				try {
					tween.Restart ();
					tween.Play ();
				} catch (UnityException e) {
					Debug.LogWarning ("Tween ERROR : " + e.ToString ());
				}
			}
		} 
		// case no tween but a delay
		else if (hideDelay != 0) 
		{
			Invoke("onHideFinished",hideDelay);
		}
		// case no tween and no delay
		else 
		{
			onHideFinished();
		}
	}
	protected virtual void addSubElementsHideTweens (){}

	void onHideFinished()
	{
		Debug.Log ("HIDE FINISHED (" + gameObject.name + ")");
		killHideTweens ();
		SetActive(false);
		onHideFinishedImpl ();
	}
	protected virtual void onHideFinishedImpl (){}



	
	/**
 	 * -------------------------------------- TWEENS HELPERS ----------------------------------------
	 */

	void killShowTweens()
	{
		foreach (Tween tween in showTweenList) 
		{
			tween.Kill();
			//tween.Pause();
		}
		isShowing = false;
	}

	void killHideTweens()
	{
		CancelInvoke("onHideFinished"); // kill possible invoke
		foreach (Tween tween in hideTweenList) 
		{
			tween.Kill();
			//tween.Restart();
			//tween.Pause();
		}
		isHiding = false;
	}

	// add show anim to the list
	protected Tween addShowAnim( Tween anim )
	{
		showTweenList.Add (anim);
		//anim.ignoreTimeScale = true;
		//anim.steeperCurves = true;
		anim.Pause ();
		return anim;
		
	}
	// add hide anim to the list
	protected Tween addHideAnim( Tween anim )
	{
		hideTweenList.Add (anim);
		//anim.ignoreTimeScale = true;
		//anim.steeperCurves = true;
		anim.Pause ();
		return anim;
	}



	/**
 	 * -------------------------------------- ANIMATION HELPERS ----------------------------------------
	 */


	// ALPHA
	Tween alphaFrom( float fromValue, float duration, float delay = 0.0f )
	{
		float endValue = initAlpha;
		canvasGroup.alpha = fromValue;
		return DOTween.To(()=> canvasGroup.alpha, x=> canvasGroup.alpha = x, endValue, duration).SetDelay(delay).SetEase(showEase);//.SetAutoKill(false);
	}
	Tween alphaTo( float toValue, float duration, float delay = 0.0f )
	{
		return DOTween.To(()=> canvasGroup.alpha, x=> canvasGroup.alpha = x, toValue, duration).SetDelay(delay).SetEase(hideEase);//.SetAutoKill(false);
	}


	// MOVE
	Tween moveBy( Vector2 byValues, float duration, float delay = 0.0f )
	{
		Vector2 endValue = initPos;
		rectTransform.anchoredPosition = new Vector2 (endValue.x + byValues.x, endValue.y + byValues.y);
		return DOTween.To(()=> rectTransform.anchoredPosition, x=> rectTransform.anchoredPosition = x, endValue, duration).SetDelay(delay).SetEase(showEase);//.SetAutoKill(false);
	}
	Tween moveAdd( Vector2 addValues, float duration, float delay = 0.0f )
	{
		Vector2 newPos = new Vector2 (rectTransform.anchoredPosition.x + addValues.x, rectTransform.anchoredPosition.y + addValues.y);
		return DOTween.To(()=> rectTransform.anchoredPosition, x=> rectTransform.anchoredPosition = x, newPos, duration).SetDelay(delay).SetEase(hideEase);//.SetAutoKill(false);
	}


	// SCALE
	Tween scaleFrom( Vector3 scaleFromVal, float duration, float delay = 0.0f )
	{
		Vector3 endValue = initScale;
		transform.localScale = scaleFromVal;
		return DOTween.To(()=> transform.localScale, x=> transform.localScale = x, endValue, duration).SetDelay(delay).SetEase(showEase);//.SetAutoKill(false);
	}
	Tween scaleTo( Vector3 scaleToValue, float duration, float delay = 0.0f )
	{
		return DOTween.To(()=> transform.localScale, x=> transform.localScale = x, scaleToValue, duration).SetDelay(delay).SetEase(hideEase);//.SetAutoKill(false);
	}
}
