﻿using UnityEngine;
using System.Collections;

public class ConnectionCheck : MonoBehaviour {

	// status
	public delegate void boolDelegate( bool isConnected );
	public static boolDelegate connectionChange;

	// private
	private WWW www;
	private bool connected = false;

	// Use this for initialization
	void Start () {
		www = new WWW("www.google.com"); 
		StartCoroutine(checkConnection());
	}
	
	IEnumerator checkConnection()
	{
		yield return www;
		bool previousConnectionStatus = connected;
		
		if(www.error != null)
		{
			connected = false;
			if(previousConnectionStatus && connectionChange != null) connectionChange(false);

			Debug.Log("faild to connect to internet, trying after 2 seconds.");
			yield return new WaitForSeconds(2);// trying again after 2 sec
			StartCoroutine(checkConnection());
		}else
		{
			connected = true;
			if(!previousConnectionStatus && connectionChange != null) connectionChange(true);

			Debug.Log("connected to internet");
			// do somthing, play sound effect for example

			yield return new WaitForSeconds(5);// recheck if the internet still exists after 5 sec
			StartCoroutine(checkConnection());
			
		}
		
	}
}
