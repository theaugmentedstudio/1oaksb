﻿using UnityEngine;
using System.Collections;

public class GAHelper {


	public static void LogScreen( string screenName )
	{
		if(GoogleAnalyticsV3.instance != null)
		{
			GoogleAnalyticsV3.instance.LogScreen(screenName);
		}
	}

	public static void LogEvent( string eventCategory, string eventAction, string eventLabel )
	{
		if(GoogleAnalyticsV3.instance != null)
		{
			GoogleAnalyticsV3.instance.LogEvent(eventCategory, eventAction, eventLabel, 0);
		}
	}
}
