﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpriteFader : MonoBehaviour
{

	// INSPECTOR 
	public SpriteRenderer spriteRenderer;

	// DATA
	Tween fadeTween;


	/* -------------------------------------- mono ---------------------------------------- */

	void Awake()
	{
		init ();
	}
	//void Start(){}
	//void Update(){}
	


	/* -------------------------------------- CrossFade ---------------------------------------- */


	public Tween CrossFade( float alphaTo, float duration, float delay )
	{
		if (fadeTween != null) {
			DOTween.Kill(fadeTween);
		}

		// set end color
		Color endColor = spriteRenderer.color;
		endColor.a = alphaTo;
		fadeTween = DOTween.To(()=> spriteRenderer.color, x=> spriteRenderer.color = x, endColor, duration).SetDelay(delay).SetEase(Ease.OutCubic);

		// return tween
		return fadeTween;
	}

	public void SetAlpha( float alpha )
	{
		Color endColor = spriteRenderer.color;
		endColor.a = alpha;
		spriteRenderer.color = endColor;
	}


	public void SetActive( bool flag )
	{
		gameObject.SetActive (flag);
	}



	/* -------------------------------------- init ---------------------------------------- */
	
	void init()
	{
		if (!spriteRenderer) {
			spriteRenderer = GetComponent<SpriteRenderer>();
			if(!spriteRenderer)
			{
				Debug.LogWarning("SpriteFader error : no spriteRenderer selected");
			}
		}
	}

}
