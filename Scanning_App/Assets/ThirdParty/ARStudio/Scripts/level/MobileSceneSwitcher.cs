﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MobileSceneSwitcher : MonoBehaviour 
{

	void Awake()
	{		
		if (DeviceInfo.deviceType == DeviceInfo.DeviceType.iPad) 
			Application.LoadLevel("BelveLights_ipad");
		else 
			Application.LoadLevel("BelveLights_iphone");
	}
	

}