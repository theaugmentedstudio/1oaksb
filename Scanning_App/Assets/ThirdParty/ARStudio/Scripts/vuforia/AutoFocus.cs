using UnityEngine;
using System;
using System.Collections;

#if VUFORIA_SAMPLE_ORIENTATION_SETTINGS

namespace Vuforia
{
	public class AutoFocus : MonoBehaviour
	{

		public void TriggerAutoFocus()
		{
			if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO))
				Debug.Log("Camera focus trigger success");
		}
	}
}

#endif
