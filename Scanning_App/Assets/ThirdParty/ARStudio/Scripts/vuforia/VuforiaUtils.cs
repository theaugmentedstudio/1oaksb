using UnityEngine;
using System;

#if VUFORIA_SAMPLE_ORIENTATION_SETTINGS

namespace Vuforia
{
	public class VuforiaUtils
	{
		public static void SwitchVuforiaCamera()
		{
			CameraDevice.CameraDirection cd = CameraDevice.Instance.GetCameraDirection();
			if(cd != CameraDevice.CameraDirection.CAMERA_FRONT )
			{
				SetCameraDirection(CameraDevice.CameraDirection.CAMERA_FRONT);
			}
			else
			{
				SetCameraDirection(CameraDevice.CameraDirection.CAMERA_BACK);
			}	
		}


		public static void TriggerAutoFocus()
		{
			if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO))
				Debug.Log("Camera focus trigger success");
		}


		public static bool IsVuforiaCreated()
		{
			return (VuforiaBehaviour.Instance != null);
		}

		/*
		bool isAutoFocusSupported  = CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		Debug.Log ("isAutoFocusSupported = " + isAutoFocusSupported);
		if (!isAutoFocusSupported) {
			CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		}
		*/



		public static void SetVuforiaActive( bool flag )
		{
			Debug.Log ("VuforiaUtils.SetVuforiaActive : " + flag);
			//VuforiaUtils.enableFlareLayer (flag);

			if(VuforiaBehaviour.Instance != null) VuforiaBehaviour.Instance.enabled = flag;
		

			//if(flag) VuforiaUtils.enableFlareLayer (flag);
		}



		public static void SetCameraDirection(CameraDevice.CameraDirection direction)
		{
			CameraDevice.CameraDirection cd = CameraDevice.Instance.GetCameraDirection();
			if (cd != direction) {

				// stop current camera
				CameraDevice.Instance.Stop();
				CameraDevice.Instance.Deinit();
				VuforiaRenderer.Instance.ClearVideoBackgroundConfig();

				// switch camera
				CameraDevice.Instance.Init(direction);

				// reset camera
				CameraDevice.Instance.SelectVideoMode(CameraDevice.CameraDeviceMode.MODE_DEFAULT);
				CameraDevice.Instance.Start();
			}
		}
		
		
		// http://answers.unity3d.com/questions/17982/how-can-i-add-flare-layer-via-code.html
		public static void enableFlareLayer( bool flag )
		{
			try
			{
				Camera ARCamera = Camera.main;
				if(!ARCamera || !ARCamera.gameObject.activeInHierarchy) return;

				Behaviour flareLayer = (Behaviour)ARCamera.GetComponent ("FlareLayer"); // flareLayer do not have C# class access
				if (flareLayer != null) flareLayer.enabled = flag;
			}
			catch(Exception e)
			{
				Debug.LogWarning("VuforiaUtils.enableFlareLayer issue : "+e.ToString());
			}
		}



		private bool stopRunningObjectTracker()
		{
			bool needsObjectTrackerRestart = false;
			
			ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			if (objectTracker != null)
			{
				if (objectTracker.IsActive)
				{
					objectTracker.Stop();
					needsObjectTrackerRestart = true;
				}
			}
			return needsObjectTrackerRestart;
		}
		
		private bool restartRunningObjectTracker()
		{
			bool hasObjectTrackerRestarted = false;
			
			ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			if (objectTracker != null)
			{
				if (!objectTracker.IsActive)
				{
					hasObjectTrackerRestarted = objectTracker.Start();
				}
			}
			return hasObjectTrackerRestarted;
		}
		
		private void ResetCameraFacingToBack()
		{
			bool needsObjectTrackerRestart = stopRunningObjectTracker();
			
			CameraDevice.Instance.Stop();
			CameraDevice.Instance.Init(CameraDevice.CameraDirection.CAMERA_BACK);
			CameraDevice.Instance.Start();
			
			if (needsObjectTrackerRestart)
				restartRunningObjectTracker();
		}
		
		private bool ChangeCameraDirection(CameraDevice.CameraDirection direction)
		{
			bool directionSupported = false;
			
			bool needsObjectTrackerRestart = stopRunningObjectTracker();
			
			CameraDevice.Instance.Stop();
			CameraDevice.Instance.Deinit();
			if (CameraDevice.Instance.Init(direction))
			{
				directionSupported = true;
			}
			CameraDevice.Instance.Start();
			
			if (needsObjectTrackerRestart)
				restartRunningObjectTracker();
			
			return directionSupported;
		}

		private bool EnableContinuousAutoFocus()
		{
			return CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		}


		/*
		private IEnumerator TriggerAutoFocusAndEnableContinuousFocusIfSet()
		{
			//triggers a single autofocus operation 
			if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO))
			{
				this.View.FocusMode = CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO;
			}
			
			yield return new WaitForSeconds(1.0f);
			
			//continuous focus mode is turned back on 
			if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO))
			{
				this.View.FocusMode = CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO;
			}
			
			Debug.Log(this.View.FocusMode);
			
		}
		*/



		/*
		// Start the camera and trackers and update the camera projection
		public static void StartQCAR()
		{
			Debug.Log("StartQCAR");
			
			CameraDevice.Instance.Init(CameraDirection);
			CameraDevice.Instance.SelectVideoMode(CameraDeviceModeSetting);
			CameraDevice.Instance.Start();
			
			if (TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER) != null)
			{
				TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER).Start();
			}
			
			if (TrackerManager.Instance.GetTracker(Tracker.Type.IMAGE_TRACKER) != null)
			{
				TrackerManager.Instance.GetTracker(Tracker.Type.IMAGE_TRACKER).Start();
			}
			
			ScreenOrientation surfaceOrientation = (ScreenOrientation)QCARWrapper.Instance.GetSurfaceOrientation();
			QCARUnity UpdateProjection(surfaceOrientation);
		}
		
		
		// Stop the trackers, stop and deinit the camera
		public static void StopQCAR()
		{
			Debug.Log("StopQCAR");
			
			if (TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER) != null)
			{
				TrackerManager.Instance.GetTracker(Tracker.Type.MARKER_TRACKER).Stop();
			}
			
			if (TrackerManager.Instance.GetTracker(Tracker.Type.IMAGE_TRACKER) != null)
			{
				TrackerManager.Instance.GetTracker(Tracker.Type.IMAGE_TRACKER).Stop();
			}
			
			if (TrackerManager.Instance.GetTracker(Tracker.Type.TEXT_TRACKER) != null)
			{
				TrackerManager.Instance.GetTracker(Tracker.Type.TEXT_TRACKER).Stop();
			}
			CameraDevice.Instance.Stop();
			CameraDevice.Instance.Deinit();
			
			QCARRenderer.Instance.ClearVideoBackgroundConfig();
		}
		*/
	}
}

#endif
