﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextureUtils : MonoBehaviour {

	public static Sprite Texture2DToSprite(Texture2D texture) {
		if(texture!=null) {
			return Sprite.Create(texture, new Rect(0,0,texture.width, texture.height), new Vector2(0.5f,0.5f));
		}
		return null;
	}



	public static Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) 
	{
		Texture2D result = new Texture2D(targetWidth,targetHeight,source.format,true);
		Color[] rpixels=result.GetPixels(0);
		float incX=((float)1/source.width)*((float)source.width/targetWidth);
		float incY=((float)1/source.height)*((float)source.height/targetHeight);
		for(int px=0; px<rpixels.Length; px++) {
			rpixels[px] = source.GetPixelBilinear(incX*((float)px%targetWidth),
			                                      incY*((float)Mathf.Floor(px/targetWidth)));
		}
		result.SetPixels(rpixels,0);
		result.Apply();
		return result;
	}

}
