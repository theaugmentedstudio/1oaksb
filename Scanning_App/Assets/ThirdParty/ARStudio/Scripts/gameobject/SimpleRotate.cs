using UnityEngine;
using System.Collections;

public class SimpleRotate : MonoBehaviour 
{
	public float maxSpeed = 10; 
	public float minSpeed = 2; 

	float speed=0;
	float direction = 1;

	void Start()
	{
		speed = minSpeed;
	}

	void Update()
	{
		// 3D ROTATE
		//transform.Rotate (60 * Time.deltaTime,60 * Time.deltaTime, 60 * Time.deltaTime);

		// 2D ROTATE

		speed += 1*direction;
		if (speed > maxSpeed) {
			speed = maxSpeed; 
			direction = -1;
		}
		else if( speed < minSpeed ) 
		{
			speed = minSpeed;
			direction = 1;
		}

		transform.Rotate (0,0, -speed );// * Time.deltaTime);
	}
	
}
