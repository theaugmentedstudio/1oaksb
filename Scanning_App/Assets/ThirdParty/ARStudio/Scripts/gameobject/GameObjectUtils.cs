﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectUtils 
{
	/**
	 *  destroy all gameobject children
	 */
	public static void DestroyAllChildren(GameObject go) 
	{
		if(go == null || go.transform == null) return;
		
		
		// METHOD 1
		/*
		int childs = go.transform.childCount;
		for (int i = childs - 1; i >= 0; i--)
		{
			GameObject.Destroy(go.transform.GetChild(i).gameObject);
		}
		*/
		
		// METHOD 2
		List<GameObject> children = new List<GameObject>();
		foreach (Transform child in go.transform) children.Add(child.gameObject); // Store it in temp list otherwise indexes are changing
		foreach (GameObject child in children) GameObject.Destroy(child);
	}


	/**
	 *  fin an object in children, even if disabled
	 */
	public static GameObject FindChildWithTag(GameObject go, string tag) 
	{
		if(go == null || go.transform == null) return null;

		//List<GameObject> children = new List<GameObject>();
		foreach (Transform child in go.transform) {
			if(child.gameObject.tag == tag) return child.gameObject;
		}
		return null;
	}


	///////////////////////////////////////////////////////////
	// Essentially a reimplementation of 
	// GameObject.GetComponentInChildren< T >()
	// Major difference is that this DOES NOT skip deactivated 
	// game objects
	///////////////////////////////////////////////////////////
	static public TType GetComponentInChildren< TType  >( GameObject objRoot ) where TType : Component
	{
		// if we don't find the component in this object 
		// recursively iterate children until we do
		TType tRetComponent = objRoot.GetComponent< TType >();                
		
		if( null == tRetComponent )
		{
			// transform is what makes the hierarchy of GameObjects, so 
			// need to access it to iterate children
			Transform     trnsRoot        = objRoot.transform;
			int         iNumChildren     = trnsRoot.childCount;
			
			// could have used foreach(), but it causes GC churn
			for( int iChild = 0; iChild < iNumChildren; ++iChild )
			{
				// recursive call to this function for each child
				// break out of the loop and return as soon as we find 
				// a component of the specified type
				tRetComponent = GetComponentInChildren< TType >( trnsRoot.GetChild( iChild ).gameObject );
				if( null != tRetComponent )
				{
					break;
				}
			}
		}
		
		return tRetComponent;
	}
}