using UnityEngine;
using System.Collections;

public class LayerUtils 
{

	// set layers on all children
	public static void SetLayerOnAllRecursive(GameObject obj, int layer) 
	{
		obj.layer = layer;
		foreach (Transform child in obj.transform) {
			SetLayerOnAllRecursive(child.gameObject, layer);
		}
	}



	public static void ChangeSpriterendererSortingLayer( GameObject obj, string layerName )
	{
		SpriteRenderer[] sprites = obj.GetComponentsInChildren<SpriteRenderer> ();
		if (sprites == null ) return;

		// apply sorting layer
		for (int i = 0; i < sprites.Length; i++) {
			sprites[i].sortingLayerName = layerName;
		}
	}
}
