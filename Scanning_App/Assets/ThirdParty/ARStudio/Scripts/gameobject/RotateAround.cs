﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour {

	// INSPECTOR
	public Vector3 rotationMask = new Vector3(1, 1, 1); //which axes to rotate around
	public float rotationSpeed = 5.0f; //degrees per second
	public Transform rotateAroundObject;


	///////////////////////////////////////////////////////////////////////////////////
	void FixedUpdate() 
	{
		if (rotateAroundObject != null) {//If true in the inspector orbit <rotateAroundObject>:
			transform.RotateAround(rotateAroundObject.transform.position, rotationMask, rotationSpeed * Time.deltaTime);
		}
		else {//not set -> rotate around own axis/axes:
			transform.Rotate(new Vector3(
				rotationMask.x * rotationSpeed * Time.deltaTime,
				rotationMask.y * rotationSpeed * Time.deltaTime,
				rotationMask.z * rotationSpeed * Time.deltaTime));
		}
	}
}
