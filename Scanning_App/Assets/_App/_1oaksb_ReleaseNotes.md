#Release notes

=========== CHECK WHILE RELEASE ===========

- ne pas oublier d'enlever le reporter avant la release (ou d'augmenter le nombre de cercles)
- check all TODO in code!
- remove debug values!!!!
- REMOVE GENERATE CONTENT FILE ONLINE !!!!


=========== WIP ===========

- save csv of all user checkins/checkout?
- STRESS TEST DATABASE !!!
- pass camera with webcamTexture and use this code to make the autofocus : https://forum.unity3d.com/threads/webcamtexture-on-android-focus-mode-fix.327956/
	- or develop plugin : http://stackoverflow.com/questions/19076316/how-to-ask-webcam-to-auto-focus-with-unity3d


=========== CURRENT VERSION ===========


##0.0.1 ( build 0 )
*19/01/2017*

- copy from STFS last version


------------ WIP ----------------

- change icon
- change splash
- change background
- change all STFS in code by barstool
- **Fix** Sometimes scanning doesn't work, we should also have something more visual when scanning occurs... now this is just the bar on bottom right. something is not really fine with the process.
- try to reduce the scanning zone so it's easier to scan a small part of screen
- use correct keystore for generation
- check to speed up AR camera apparition
- make GIT folder
- confirm password with client

=========== PREVIOUS VERSION ===========


        
