﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class DebugVo 
{

	// GENERAL
	public static readonly bool CLEAR_PLAYERPREFS_AT_START 	= true;		// at start clear cookies EDIT: we do not use the cookie for user role for now.
	public static readonly bool DO_NOT_SEND_DEVMAIL			= false;	// do not send dev mail
	public static readonly string FORCE_LANG		 		= null;		// log form parameters sent // Language.FR.ToString();

	// SERVICES
	public static readonly bool USE_DEV_SERVER				= false;	// Use dev server (not production) !! to be set to false when going to production
	public static readonly bool SIMULATE_SERVER_DELAY		= false;	// simulate longer respond time from server
	public static readonly bool DO_NOT_LOG_FORM_PARAMS		= false;	// log form parameters sent
	public static readonly bool USE_LINESPACE_CODE_REPLACE	= true;		// error with initial order, replace linespace by space


	// SUPERTFS
	public static readonly bool DO_NOT_CHECK_DATE		 	= false;		// do not block checkin if wrong date
}

