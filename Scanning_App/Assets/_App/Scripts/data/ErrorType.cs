﻿using System.Collections;
using System.Collections.Generic;

public class ErrorCode
{
	public Dictionary<int, ErrorType> errorMap = new Dictionary<int, ErrorType> {
		// general
		{ 1000, ErrorType.UNKNOW_ERROR}
		,{ 1001, ErrorType.NOT_LOGGED_IN}	// unauthorized : not logged in
		,{ 1002, ErrorType.SESSION_EXPIRED}	
		,{ 1003, ErrorType.FORBIDDEN}	// no access for this phone

		// Collectgame
		,{ 4001, ErrorType.OUTSIDE_OF_BOUNDARIES} // Requested location is outside of authorized boundaries
		,{ 4101, ErrorType.NO_MORE_DIGS} //No more dig available
		,{ 4102, ErrorType.ALREADY_DUG} //Already dug
	};
}

public enum ErrorType 
{
	// global
	UNKNOW_ERROR
	,NOT_LOGGED_IN
	,SESSION_EXPIRED
	,FORBIDDEN
	,NO_CONNECTION
	
	// register
	,PHONE_INVALID
	,CODE_INVALID
	
	// login

	// collect game
	,OUTSIDE_OF_BOUNDARIES
	,NO_MORE_DIGS
	,ALREADY_DUG
	
}