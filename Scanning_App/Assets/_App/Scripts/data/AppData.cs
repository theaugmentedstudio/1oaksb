﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class AppData 
{
	// app infos
	public static AppVo appVo = new AppVo ();

	// current session infos
	public static SessionVo sessionVo = new SessionVo ();

	// services infos
	public static ServiceVo serviceVo = new ServiceVo ();

	// user datas
	//public static UserVo userVo = new UserVo ();

	// order datas
	public static OrderVo orderVo = new OrderVo ();
}

[Serializable]
public class AppVo
{
	public int refWidth = 1080;
	public int refHeight = 1920;
	static string _version;
	public string buildVersion
	{
		get 
		{  
			if (_version == null) { _version = CurrentBundleVersion.version + " ("+CurrentBundleVersion.buildNumber+")"; }
			return _version;
		}
	}
}

public enum SESSION_ROLE
{
	// from lowest to highest
	NONE,
    HOSTESS, // can only activate to day of the event (on place)
	CHECKIN, // check-in the ticket, whatever the day 
	ADMIN   // can check-in and check-out
}

[Serializable]
public class SessionVo
{
	public string lang ;
	public bool activated; 	// is this applicaiton already activated
	public SESSION_ROLE sessionRole;	// current session role
    public const string access_hostess = "7428b8c61e92fad1454dd82146be4508def96b0d";
    public const string access_checkin = "6914e6100579bd408bd7a0f1ad7181a867f30793";
    public const string access_admin = "4e909af2e4584996de6707b73a4468c946bed30c"; // activation key

    // token for secure communication (checkout)
    public const string token_prefix = "1oaksb";
    public const string token_sufix = "2017";
}

[Serializable]
public class ServiceVo
{
	public string serverUrl
	{
		get{
			if(DebugVo.USE_DEV_SERVER) 
				return "http://localhost:8888/1oaksb/";
			else 
                return "https://1oaksb.com/scan_api/";
		}
	}

	public string debugUrl
	{
		get{
			if(DebugVo.USE_DEV_SERVER) 
                return "https://1oaksb.com/scan_api/";
			else 
                return "https://1oaksb.com/scan_api/";
		}
	}

	// login/register
	public string getOrder = "getOrder.php";
	public string checkin = "checkin.php";
	public string checkout = "checkout_adm.php";

	//public string loginHandler = "login.php";
	//public string registerHandler = "register.php";
}


/* -------------------------------------- USER ---------------------------------------- */


[Serializable]
public class UserVo
{
	public string id;
	public string firstname;
	public string lastname;
	public string email;
	public string phone;
	public string adress;

	/*
	public void fill(Dictionary<string, object> JSONObj)
	{
		id = (string) JSONObj["id"];
		firstname = (string) JSONObj["firstname"];
		lastname = (string) JSONObj["lastname"];
		email = (string) JSONObj["email"];
		phone = (string) JSONObj["phone"];

	}
	*/
}


/* -------------------------------------- ORDER ---------------------------------------- */

[Serializable]
public class OrderVo
{
	public string barcode;
	public string orderID;
	public string ticketID;
	public bool used;
	public string dateCheck;
	public UserVo user;
	public string productDescription;
	public string productSku;
	public string productVariation;
	public string orderDate;
	public string eventDate;
	public string checkinDate;


	// is the event over
	public bool isEventAlreadyOver{	get { return (dateCheck == "past");}}

	// is the event not yet started
	public bool isEventNotYetStarted{	get { return (dateCheck == "later");}}

	// is event running
	public bool isEventNow{	get { return (dateCheck == "ok");}}


	public void fill(Dictionary<string, object> JSONObj)
	{
		// fill infos
		//Dictionary<string, object> userProfile = (Dictionary<string, object>) userObj["profile"];
		barcode = (string) JSONObj["ID"];
		orderID = (string) JSONObj["order_id"];
		used = ((string) JSONObj["used"] == "1") ? true : false;
		dateCheck = (string)JSONObj ["dateCheck"];

		// product
		productDescription = (string) JSONObj["product_desc"];
		productVariation = (string) JSONObj["product_variation"];
		productSku = (string) JSONObj["product_sku"];

		// user
		user = new UserVo();
		user.firstname = (string) JSONObj["user_firstname"];
		user.lastname = (string) JSONObj["user_lastname"];
		user.phone = (string) JSONObj["user_phone"];
		user.adress = (string) JSONObj["user_adress"];

		// date
		eventDate = (string) JSONObj["date_event"];
		orderDate = (string) JSONObj["date_order"];
		checkinDate = (string) JSONObj["date_checkin"];

		// fill user
		/*
		if (JSONObj.ContainsKey ("user")) {
			// user is a string formated json, we need to deserialize it
			Dictionary<string, object> userObj = (Dictionary<string, object>) MiniJSON.Json.Deserialize(JSONObj["user"].ToString());
			user = new UserVo();
			user.fill(userObj);
		}
		*/

		// fill product

	}
}


/*
public class Wallet
{
	public int coins = 0;
	public int diamonds = 0;
	
	// shovels
	public int currentShovel = 0;
	public int woodShovel = 0;
	public int silverShovel = 0;
	public int goldShovel = 0;

	public void fillShovels(Dictionary<string, object> shovelsObj)
	{
		woodShovel = int.Parse (shovelsObj ["WOOD"].ToString ());
		silverShovel = int.Parse (shovelsObj ["SILVER"].ToString ());
		goldShovel = int.Parse (shovelsObj ["GOLD"].ToString ());
	}

	public int numShovels
	{
		get
		{
			return woodShovel + silverShovel + goldShovel;
		}
	}
}

public enum DigType
{
	EMPTY,
	COIN,
	DIAMOND,
	TREASURE
}

public class DigVo
{
	public string markId;

	// result
	public string type;
	public string name;
	public string description;
	public string image;
	public int amount;

	public void fill(Dictionary<string, object> gainObj)
	{
		//"gain":{"name":"random_diamond","description":"treasure_random_diamond","type":"DIAMOND","weight":0,"image":"","parameters":{"amount":1}}}}
		// fill infos
		type = (string) gainObj["type"];
		name = (string) gainObj["name"];
		description = (string) gainObj["description"];
		image = (string) gainObj["image"];

		// parameters
		if (gainObj ["parameters"] != null) 
		{
			Dictionary<string, object>  paramObj = (Dictionary<string, object>) gainObj["parameters"]; 
			amount = int.Parse(paramObj["amount"].ToString());
		}
	}
}

[Serializable]
public class SocialVo
{
	public string FBID;
	public string TwitterID;
}
*/


