﻿using UnityEngine;
using System.IO;
using System.Collections;
//using System.Collections.Generic

public class Signals {

	/**
 	 * -------------------------------------- SIGNAL PATTERNS ----------------------------------------
	 */

	// global signal delegate = Type of possible signals
	public delegate void voidEventDelegate();
	public delegate void stringEventDelegate(string msg);
	public delegate void intEventDelegate(int val);

	/**
 	 * -------------------------------------- SIGNALS ----------------------------------------
	 */

	// global signals
	public static voidEventDelegate BACK_TO_DASHBOARD;	// return to dashboard

}
