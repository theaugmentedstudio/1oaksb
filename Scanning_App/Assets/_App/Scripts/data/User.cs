﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using MiniJSON;

public class Usr
{
	public string name { get; set; }
	public string email { get; set; }
	public int lastSession { get; set; }
	

	/**
	 *  EXAMPLE OF AUTO CREATION OF DATA
	 */
	public static Company GetData()
	{
		return new Company()
		{
			Title = "Company Ltd",
			Employees = new List<Employee>()
			{
				new Employee(){ Name = "Mark CEO", EmployeeType = EmployeeType.CEO },
				new Employee(){ Name = "Matija Božičević", EmployeeType = EmployeeType.Developer },
				new Employee(){ Name = "Steve Developer", EmployeeType = EmployeeType.Developer}
			}
		};
	}
	
	public static string save()
	{
		Company comp = Usr.GetData();
		
		// OLD
		// Pass "company" object for conversion object to JSON string
		//string json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(comp);
		// Write that JSON to txt file
		//File.WriteAllText(Environment.CurrentDirectory + @"\JSON.txt", json)
		/*{"Title":"Company Ltd","Employees":[{"Name":"Mark CEO","EmployeeType":0},{"Name":"Matija 
		Božičević","EmployeeType":1},{"Name":"Steve Developer","EmployeeType":1}]}*/
		
		
		string str = Json.Serialize(comp);
		PlayerPrefsSerializer.Save("coucou", comp);
		
		//var dict = Json.Deserialize(jsonString) as Dictionary<string,object>;
		//
		//          Debug.Log("deserialized: " + dict.GetType());
		//          Debug.Log("dict['array'][0]: " + ((List<object>) dict["array"])[0]);
		//          Debug.Log("dict['string']: " + (string) dict["string"]);
		//          Debug.Log("dict['float']: " + (double) dict["float"]); // floats come out as doubles
		//          Debug.Log("dict['int']: " + (long) dict["int"]); // ints come out as longs
		//          Debug.Log("dict['unicode']: " + (string) dict["unicode"]);
		//
		//          var str = Json.Serialize(dict);
		//
		//          Debug.Log("serialized: " + str);
		return str;
	}
	
	public static void load()
	{
		//string json = File.ReadAllText(Environment.CurrentDirectory + @"\JSON.txt");
		//Company company = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Company>(json);
	}
}

[System.Serializable]
public class Company
{
	public string Title { get; set; }
	public List<Employee> Employees { get; set; }
}

/// <summary>
/// Employee info
/// </summary>
[System.Serializable]
public class Employee
{
	public string Name { get; set; }
	public EmployeeType EmployeeType { get; set; }
}

/// <summary>
/// Types of employees
/// (just to se how JSON deals with enums!)
/// </summary>
public enum EmployeeType
{
	CEO,
	Developer
}

