﻿//------------------------------------------------------------------------------
// Copyright Anthony De Brackeleire 2014
//------------------------------------------------------------------------------
using System;
using System.Collections;
using UnityEngine;

public class GUIManager : Singleton<GUIManager>
{
	/**
 	 * -------------------------------------- view/LIST ----------------------------------------
	 */

	public enum Views
	{
		None,

		// ui
		ModalView,
		HeaderView,
		LoadingView,

		// views
		ActivateView,
		HomeView,
		ScanView,
		DetailView,
		CompleteView,
	}

	/**
 	 * -------------------------------------- EVENTS/DELEGATE ----------------------------------------
	 */
	
	public delegate void ReadyAction();
	public static event ReadyAction ON_READY;

	/**
 	 * -------------------------------------- PROPERTIES ----------------------------------------
	 */

	GameObject uiRoot;
	GameObject[] viewList;

	public Camera UICamera;

	[HideInInspector]
	public ViewBehavior currentView; 
	[HideInInspector]
	public bool allowBackAction = true; // can be updated at runtime


	static bool ready = false;
	public static bool IsReady {
		get{ return ready; }
	}
	//int waitingView = -1;

	/**
 	 * -------------------------------------- MONOBEHAVIOR ----------------------------------------
	 */
	//void Awake(){ StartCoroutine (init ());}
	//void Start(){}
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
			OnBackButtonHandler ();
	}
	//void LateUpdate(){}
	
	/**
 	 * -------------------------------------- INIT ----------------------------------------
	 */
	
	protected override void Awake ()
	{
		base.Awake ();
	}
	
	
	public void Start()
	{
		uiRoot = GameObject.FindGameObjectWithTag("UI");

		if (!uiRoot) StartCoroutine (UISceneLoad ());
		else initViews ();
	}
	IEnumerator UISceneLoad()
	{
		Application.LoadLevelAdditive("UIScene");

		yield return null; // wait till the end of frame
		uiRoot = GameObject.FindGameObjectWithTag("UI");
		//uiRoot.transform.parent = this.transform;

		// be sure to remove camera as we will use the one from the current scene
		UICamera = GameObject.FindGameObjectWithTag ("UICamera").GetComponent<Camera> ();
		//if(uiCam != null) uiCam.SetActive(false); // This previously disabled uicam.. which we don't want

		// be sure to keep ui root accross scenes
		DontDestroyOnLoad(uiRoot);

		initViews ();
	}


	/**
	 *  at start, recover all views by tag
	 *  store them in view array
	 *  disable them
	 */
	void initViews()
	{
		// find and store views
		int numOfViews = Enum.GetNames(typeof(Views)).Length;

		viewList = new GameObject[numOfViews-1]; //-1 because of none
		Views[] views = (Views[]) Enum.GetValues(typeof(Views));
		for (int i = 1; i < viewList.Length; i++) {
			GameObject go = GameObjectUtils.FindChildWithTag(uiRoot, views[i].ToString());
			if(!go) Debug.LogError("view with tag : '" + views[i].ToString() + "' was not found in hierarchy");
			//else Debug.Log("---view with tag : '" + views[i].ToString() + "' was found in hierarchy"); // TODO : comment this, this is for debug purpose
			viewList[i] = go;
			
			//disable views if it is active
			go.SetActive(false);
		}

		ready = true;
		if(ON_READY != null) ON_READY();

		/*if (waitingView != -1)
			ShowView ((Views)waitingView);*/
	}


	public ViewBehavior GetView( Views view )
	{
		return viewList [(int)view].GetComponent<ViewBehavior> ();
	}



	public ViewBehavior ShowViewAsMain( Views view )
	{
		return ShowViewAsMain ( view, false );
	}
	public ViewBehavior ShowViewAsMain( Views view, bool forceShow )
	{
		//BlockUI.Block (true);
		allowBackAction = false;
		ViewBehavior newView = GetView(view);

		if (forceShow)
			newView.ForceReShow ();
		else
			newView.SetActive (true);

		currentView = newView;
		UpdateInterface ();
		Invoke ("reactivateBackHandler", 1.0f);
		return newView;
	}
	void reactivateBackHandler()
	{
		allowBackAction = true;
	}


	/*-------------------------------------- BACK BUTTON HANDLER ----------------------------*/

	public void OnBackButtonHandler()
	{
		Debug.Log ("GUIManager.OnBackButtonHandler");

		// do not allow to back if service is running
		if (ServiceManager.Running || !allowBackAction)
			return;

		Views backView = Views.None;

		// we do not keep an history here, we just modify the back page depending on current page
		if (currentView.tag == Views.ScanView.ToString ())
			backView = Views.HomeView;
		else if (currentView.tag == Views.DetailView.ToString ())
			backView = Views.ScanView;


		// display correct view
		if(backView != Views.None)
		{
			currentView.hide();
			ShowViewAsMain(backView);
		}
	}


	/*-------------------------------------- UPDATE OTHER INTERFACE ELEMENT ----------------------------*/

	void UpdateInterface()
	{
		updateBackground ();
		updateHeader ();
	}

	void updateBackground()
	{
		// update background/modal
		ViewBehavior modal = GetView (Views.ModalView);
		bool modalActive = true;
		
		// check if we need to desactive
		if( currentView.tag == Views.ScanView.ToString() )
		{
			modalActive = false;
		}
		
		// display
		if (modalActive)
		{
			if(modal.gameObject.activeSelf == false) modal.SetActive(true);
			else if(modal.IsHiding) modal.ForceReShow ();
		}
		else if (modal.gameObject.activeSelf) modal.hide();
	}
	void updateHeader()
	{
		// update background/modal
		ViewBehavior header = GetView (Views.HeaderView);
		bool headerActive = true;
		
		// check if we need to desactive
		if( currentView.tag == Views.ActivateView.ToString() )
		{
			headerActive = false;
		}

		// update back button
		bool backButtonVisible = false;
		if (currentView.tag == Views.ScanView.ToString () || currentView.tag == Views.DetailView.ToString ())
			backButtonVisible = true;

		(header as HeaderView).UpdateBackButton (backButtonVisible);


		//show or not
		if (headerActive)
		{
			if(header.gameObject.activeSelf == false) header.SetActive(true);
			else if(header.IsHiding) header.ForceReShow ();
		}
		else if (header.gameObject.activeSelf) header.hide();


	}

	/*
	public void ShowView( Views view )
	{
		Debug.Log ("GUIManager.Showview : " + view.ToString ());

		// if GUIManager is not ready, we store asked view 
		if (!ready) 
		{
			waitingView = (int) view;
			return;
		}
		waitingView = -1;


		// case the same view
		if (currentview && currentview.tag != view.ToString ())
			return;

		// hide current view
		if (currentview) currentview.SetActive (false);

		// show view
		currentview = viewList [(int)view];
		currentview.SetActive (true);
	}
	*/



	public void BlockMouse( bool flag )
	{
		//BlockUIClip.SetActive (flag);
	}
}


