﻿//------------------------------------------------------------------------------
// Copyright Anthony De Brackeleire 2014
//------------------------------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class AppCore : Singleton<AppCore>
{
	/** -------------------------------------- PROPERTIES ----------------------------------------*/

	// INSPECTOR 

	// Controllers

	

	/** -------------------------------------- MONOBEHAVIOR ----------------------------------------*/


	protected override void Awake ()
	{
		base.Awake ();

		// init dotTween
		DOTween.Init (false, false, LogBehaviour.ErrorsOnly);

		// check and load language
		/*
		if(Application.systemLanguage == SystemLanguage.French)
			LanguageManager.LoadLanguageFile (Language.FR);
		else
			LanguageManager.LoadLanguageFile (Language.EN);

		// init language pack
		if(DebugVo.FORCE_LANG != null) LanguageManager.LoadLanguageFile((Language) Enum.Parse(typeof(Language), DebugVo.FORCE_LANG));
		*/
	

		// global listeners
		//Signals.LOGOUT += onLogout;

		// set orientation
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		Screen.autorotateToLandscapeLeft = false;
		Screen.autorotateToLandscapeRight = false;


		// wait untill UI is ready
		GUIManager.ON_READY += onGuiReady;
		if (GUIManager.IsReady) onGuiReady ();


	}

	void setQuality()
	{
		#if UNITY_ANDROID
		
		Application.targetFrameRate = 60;
		
		QualitySettings.vSyncCount = 0; 
		
		QualitySettings.antiAliasing = 0;
		int qualityLevel = QualitySettings.GetQualityLevel();

		if (qualityLevel == 0)
		{
			QualitySettings.shadowCascades = 0;
			QualitySettings.shadowDistance = 15;
		}
		
		else if (qualityLevel == 5)
		{
			QualitySettings.shadowCascades = 2;
			QualitySettings.shadowDistance = 70;
		}

		
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		
		#endif
		
		
		
		#if UNITY_STANDALONE_WIN
		
		Application.targetFrameRate = 60;
		QualitySettings.vSyncCount = 1; 
		
		if (qualityLevel == 0)
		{
			QualitySettings.antiAliasing = 0;
		}
		
		if (qualityLevel == 5)
		{
			QualitySettings.antiAliasing = 8;
		}
		
		#endif
	}

	/** -------------------------------------- MUSIC HANDLING ---------------------------------------- */



	/** -------------------------------------- INIT ---------------------------------------- */

	void OnDestroy()
	{
		Debug.LogWarning ("APPCORE SHOULD NEVER BE DESTROYED !");
		GUIManager.ON_READY -= onGuiReady;
	}


	void onGuiReady()
	{	
		GUIManager.ON_READY -= onGuiReady;
		ServiceManager.Instance.init (); // init service manager when gui is ready
		initSessionDatas (); // prepare session datas

		//switch between activate view and home view
		//Debug.Log ("PlayerPrefs.GetInt : " + PlayerPrefs.GetInt ("activated"));
		if (PlayerPrefs.GetInt ("activated") > 0) {
			AppData.sessionVo.sessionRole = (SESSION_ROLE)PlayerPrefs.GetInt ("activated");
			GUIManager.Instance.ShowViewAsMain (GUIManager.Views.HomeView);
			//GUIManager.instance.GetView (GUIManager.Views.HomeView).SetActive (true);
		}
		else
			GUIManager.Instance.ShowViewAsMain(GUIManager.Views.ActivateView);
			//GUIManager.instance.GetView (GUIManager.Views.ActivateView).SetActive (true);
	}

	void initSessionDatas()
	{
		/*
		AppData.userVo.wallet.coins = 35;
		AppData.userVo.wallet.diamonds = 12;
		AppData.userVo.wallet.woodShovel = 3;
		AppData.userVo.wallet.silverShovel = 1;
		AppData.userVo.wallet.goldShovel = 0;
		*/
	}


	/** -------------------------------------- SHOW DASHBOARD ---------------------------------------- */

	/*
	void showDashboardFromSplash()
	{
		// hide splash
		GUIManager.instance.GetView (GUIManager.Views.SplashView).hide ();

		// show header
		GUIManager.instance.GetView (GUIManager.Views.HeaderView).SetActive (true);

		// show modal
		GUIManager.instance.GetView (GUIManager.Views.ModalView).SetActive (true);

		// show Dash
		GUIManager.Instance.ShowViewAsMain (GUIManager.Views.DashboardView);

	}
	*/

}


