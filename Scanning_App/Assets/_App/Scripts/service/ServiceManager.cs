﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;

public class ServiceManager : SingletonAutoCreate<ServiceManager> 
{

	/**
 	 * -------------------------------------- PROPERTIES ----------------------------------------
	 */

	static bool isRunning; // is there a service running
	public static bool Running
	{
		get{return isRunning;}
	}

	// datas
	//int maxRetry = 3;
	//int currentRetry = 0;
	ViewBehavior loadingView;
	ServiceInfo lastService;


	/**
 	 * -------------------------------------- setup loading and connection error clip (if needed) ----------------------------------------
	 */

	public void init()
	{
		loadingView = GUIManager.Instance.GetView (GUIManager.Views.LoadingView);//.GetComponent<LoadingView> ();
	}



	/**
 	 * -------------------------------------- Get ORDER details ----------------------------------------
	 */
	
	public void GetOrder( string code, Action<ServiceResult> onSuccess, Action<ServiceResult> onError = null)
	{
		logService ("=> GetOrder");

		// create form
		var form = new WWWForm();

		// remove code whitespace
		if (DebugVo.USE_LINESPACE_CODE_REPLACE) {
			code = WWW.EscapeURL (code);
			Debug.Log ("code = " + code);
			code = code.Replace ("%0a", "+");
			code = WWW.UnEscapeURL (code);
		}

		addFormField (form, "code", code);

		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.getOrder;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, form, null, onSuccess, onError );
		si.resultType = ServiceResult.ResultType.JSON; //ServiceResult.ResultType.PLAYANDGOLD;
		StartCoroutine(ExecuteService(si));

		// DEBUG
		//if(SIMULATE_SERVER) onSeedSuccess("{\"seed\":\"2945\"}");
	}


	/**
 	 * -------------------------------------- CHECKIN ----------------------------------------
	 */
	
	public void CheckIN( string code, Action<ServiceResult> onSuccess, Action<ServiceResult> onError = null)
	{
		logService ("=> CheckIN");
		
		// create form
		var form = new WWWForm();
		addFormField (form, "code", code);
		
		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.checkin;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, form, null, onSuccess, onError );
		si.resultType = ServiceResult.ResultType.JSON;//ServiceResult.ResultType.PLAYANDGOLD;
		StartCoroutine(ExecuteService(si));
		
		// DEBUG
		//if(SIMULATE_SERVER) onSeedSuccess("{\"seed\":\"2945\"}");
	}


	/**
 	 * -------------------------------------- CHECKIN ----------------------------------------
	 */
	
	public void CheckOUT( string code, Action<ServiceResult> onSuccess, Action<ServiceResult> onError = null)
	{
		logService ("=> CheckOUT");
		
		// create form
		var form = new WWWForm();
		addFormField (form, "code", code);

        string securityToken = SessionVo.token_prefix + code + SessionVo.token_sufix;
		securityToken = GetSHA1HashString (securityToken);
		addFormField (form, "token", securityToken);
		
		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.checkout;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, form, null, onSuccess, onError );
		si.resultType = ServiceResult.ResultType.JSON;//ServiceResult.ResultType.PLAYANDGOLD;
		StartCoroutine(ExecuteService(si));
		
		// DEBUG
		//if(SIMULATE_SERVER) onSeedSuccess("{\"seed\":\"2945\"}");
	}



	/**
 	 * -------------------------------------- RETRIEVE glossary ----------------------------------------
	 */
	/*
	public void GetGlossary( Action<ServiceResult> onSuccess, Action<String> onError = null)
	{
		logService ("=> GetGlossary");
		
		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.glossaryHandler;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, null,null, onSuccess, onError );
		si.resultType = ServiceResult.ResultType.PLAYANDGOLD;
		StartCoroutine(ExecuteService(si));
		
		// DEBUG
		//if(SIMULATE_SERVER) onSeedSuccess("{\"seed\":\"2945\"}");
	}
	/


GetOrder
	/**
 	 * -------------------------------------- Request seed for phone number ----------------------------------------
	 */

	/*
	public void RequestSeed( string msisdn, string locale, Action<ServiceResult> onSuccess, Action<String> onError = null)
	{
		logService ("=> RequestSeed");

		// create form
		var form = new WWWForm();
		form.AddField( "msisdn", msisdn );
		form.AddField( "locale", locale );

		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.requestSeedHandler;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, form, null, onSuccess, onError );
		si.resultType = ServiceResult.ResultType.TEXT;//ServiceResult.ResultType.PLAYANDGOLD;
		StartCoroutine(ExecuteService(si));

		// DEBUG
		//if(SIMULATE_SERVER) onSeedSuccess("{\"seed\":\"2945\"}");
	}
	*/

	/**
 	 * -------------------------------------- Validate seed ----------------------------------------
	 */

	/*
	public void VerifySeed( string seed, string msisdn, Action<ServiceResult> onSuccess, Action<String> onError = null)
	{
		logService ("=> VerifyeSeed");
		
		// create form
		var form = new WWWForm();
		// create sha1 encoded string 
		form.AddField( "seed", GetSHA1HashString(seed+"_"+msisdn));
		form.AddField( "msisdn", msisdn );

		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.verifySeedHandler;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, form, null, onSuccess, onError );
		//si.resultType = ServiceResult.ResultType.PLAYANDGOLD;
		si.resultType = ServiceResult.ResultType.TEXT;
		StartCoroutine(ExecuteService(si));
		
		// DEBUG
		//if(SIMULATE_SERVER) onRegisterSuccess("{ \"deviceId\": \"5d9f8f3d87e208930e0333db09610c2022804f9d\" }");
	}
	*/

	/**
 	 * -------------------------------------- login check ----------------------------------------
	 */

	/*
	public void LoginCheck( string deviceID, Action<ServiceResult> onSuccess, Action<String> onError = null)
	{
		logService ("=> LoginCheck");

		// create form
		var form = new WWWForm();

		form.AddField( "os", SystemInfo.operatingSystem ); 
		form.AddField( "model", SystemInfo.deviceModel ); 
		form.AddField( "version", SystemInfo.deviceType.ToString() );
		form.AddField( "device-id", deviceID );

		//var rawData = form.data;
		var headers = form.headers;
		headers ["Accept"] = "application/json";
		headers ["device-id"] = deviceID ; //"Basic " + System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes("username:password"));

		// url
		string url = AppData.serviceVo.serverUrl + AppData.serviceVo.loginHandler;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, form, headers, onSuccess, onError );
		si.tag = "login";
		si.resultType = ServiceResult.ResultType.PLAYANDGOLD;
		si.useRetryPopupSystem = true; // use retry popup system for this service
		StartCoroutine(ExecuteService(si));
		
		// DEBUG
		//if(SIMULATE_SERVER) onLoginSuccess("{ \"blablabla\": \"blablabla\" }");
	}
	*/


	/**
 	 * -------------------------------------- Get collect game configuration ----------------------------------------
	 */
	

	/*
	public void CheckFBAvailability()
	{
		logService ("DB.CheckFBAvailability");
		
		// url
		string url = Infos.serverURL + "FBCheck.php" + "?t="+GetTimestamp();

		// service execute
		ServiceInfo si = new ServiceInfo( url, null, fbResult, null );
		StartCoroutine(ExecuteService(si));
	}
	void fbResult( ServiceResult result )
	{
		logService ("DB.CheckFBAvailability result : "+result.raw);
		
		//if(result.raw == "OK") Infos.useFacebook = true;
		//else Infos.useFacebook = false;
	}
	*/

	/**
 	 * -------------------------------------- CHECK VERSION UPDATE ----------------------------------------
	 */
	/*
	public void CheckNewVersion(Action<ServiceResult> onSuccess, Action<String> onError = null)
	{
		logService ("DB.CheckNewVersion");
		
		// url
		string url = Infos.serverURL + "CheckVersion.php" + "?version="+Infos.VERSION;
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, null, onSuccess, onError );
		si.resultType = ServiceResult.ResultType.TEXT;
		StartCoroutine(ExecuteService(si));
	}
	*/



	/**
 	 * -------------------------------------- SAVE SCORE ONLINE ----------------------------------------
	 */
	/*
	public void SaveScore( int gameId, string userName, int score, int time, string date )
	{
		logService ("DB.SaveScore"); 
		
		// url
		string url = Infos.serverURL + "saveScore.php" + "?t="+GetTimestamp();
		WWWForm form = new WWWForm();
		form.AddField("gameid",gameId);
		form.AddField("username",userName);
		form.AddField("score",score);
		form.AddField("time",time);
		form.AddField("date",date);

		// service execute
		ServiceInfo si = new ServiceInfo( url, form, null, null );
		StartCoroutine(ExecuteService(si));
	}
	*/

	/**
 	 * -------------------------------------- SAVE IMAGE ----------------------------------------
	 */

	/*
	public void SaveImage(string firstname, string lastname, string email, byte[] jpgData, string imageName, bool optin, Action<ServiceResult> onSuccess, Action<String> onError )
	{
		logService ("DB.SaveImage"); 
		
		// url
		string url = Infos.serverURL + "saveImage.php" + "?t="+GetTimestamp();
		WWWForm form = new WWWForm();
		form.AddField ("firstname", firstname);
		form.AddField ("lastname", lastname);
		form.AddField ("email", email);
		int optinInt = Convert.ToInt32(optin);
		form.AddField("optin",optinInt);
		form.AddBinaryData ("Filedata", jpgData,imageName); //,"text/plain"


		// service execute
		ServiceInfo si = new ServiceInfo( url, form, onSuccess, onError );
		StartCoroutine(ExecuteService(si));
	}
	*/

	
	/**
 	 * -------------------------------------- GET HIGHSCORES ONLINE ----------------------------------------
	 */

	/*
	public void GetHighScores(Action<ServiceResult> onSuccess )
	{
		logService ("DB.GetHighScores"); 
		
		// url
		string url = Infos.serverURL + "getHighScores.php" + "?t="+GetTimestamp();
		
		// service execute
		ServiceInfo si = new ServiceInfo( url, null, onSuccess, null );
		StartCoroutine(ExecuteService(si));
	}
	*/
	
	
	
	 /**
 	 * -------------------------------------- TIMESTAMP UTIL ----------------------------------------
	 */
	
	public static String GetTimestamp()
	{	
		return DateTime.Now.ToString("yyyyMMddHHmmssffff");
	}
	
	
	/**
	 * -------------------------------------- EXECUTE SERVICE ----------------------------------------
	 */
	
	IEnumerator ExecuteService( ServiceInfo si)
	{
		// show loading view if there is one
		if(loadingView != null)
			loadingView.SetActive(true);

		// set running
		isRunning = true;

		// block UI when service execute
		BlockUI.Block (true);

		// log services 
		logService("==> Service Execute ("+si.url+")");
		lastService = si;

		// check headers
		if (si.headers == null) {
			si.headers = new Dictionary<string, string>();
			si.headers ["Accept"] = "application/json";
			//si.headers ["Cookie"] = AppData.sessionVo.sessionCookie;
		}

		// Make the call
		WWW www;
		if(si.form != null)	www = new WWW (si.url, si.form.data, si.headers);
		else www = new WWW (si.url, null , si.headers);
		
		// WAIT FOR RESULT
		yield return www;

		// added wait time for debug if needed
		if (DebugVo.SIMULATE_SERVER_DELAY)
			yield return new WaitForSeconds (3.0f);

		// result
		if(si.logResult) logService("" + www.text);

		// recover cookie on login only
		/*
		if (si.tag == "login")
		{
			if( www.responseHeaders.ContainsKey ("SET-COOKIE") )
			{
				string cookie = www.responseHeaders["SET-COOKIE"]; // [SET-COOKIE] => PHPSESSID=alkh7mcufrp5igus9f911eftb0; path=/
				cookie = ""+ cookie.Split(new string[]{";"}, StringSplitOptions.None)[0] + ";";
				//AppData.sessionVo.sessionCookie = cookie;
			}
			else
			{
				// TODO : manager error if no cookie set in headers !!
				logServiceWarning("--> ERROR : No cookie set in headers for login-check");
				//logServiceWarning("--> CurrentCookie is : " + AppData.sessionVo.sessionCookie);
			}
		}
		*/

		// LOG HTTP HEADERS
		/*
		logService ("$$$$$$$$$$ RESPONSE HEADERS $$$$$$$$$$");
		foreach (KeyValuePair<string, string> keyVal in www.responseHeaders) 
		{
			logService("["+keyVal.Key+"] => " + keyVal.Value);
		}
		logService ("$$$$$$$$$$ RESPONSE HEADERS $$$$$$$$$$");
		*/


		// PLAY AND GOLD RESULT HANDLING
		//logService ("************* SI RESULT TYPE : " + si.resultType + " & WWW.Text = " + www.text);
		/*
		if (si.resultType == ServiceResult.ResultType.PLAYANDGOLD && www.text != null )
		{
			ServiceResult result = new ServiceResult(www.text, si.resultType);
			if( result.success ) {
				// on success
				logService("===> WWW Success ("+si.url+") :");
				if( si.onSuccess != null ) si.onSuccess( result );
			}
			else serviceError(si, "HttpStatus:"+result.httpStatus + " - ErroCode: "+result.errorCode + " - Message: "+result.message);
		}
		
		// CHECK FOR ERRORS
		else 
		*/

		// create service result
		ServiceResult result = new ServiceResult(www.text, si.resultType);

		// if no error from www
		if (www.error == null)
		{

			if( result.success ) {
				// on success
				logService("===> WWW Success ("+si.url+") :");

				// unblock ui
				BlockUI.Block (false);

				// no more running
				isRunning = false;

				// log and success
				if( si.logResult) logService("" + result.raw);
				if( si.onSuccess != null ) si.onSuccess( result );

				// hide loading view
				if (loadingView != null)
					loadingView.hide ();
			}
			else serviceError(si, result );
		}

		// else we retry
		else if (si.retryCount < si.maxRetry ) 
		{
			logServiceWarning("===> WWW ERROR, we retry ("+si.retryCount+"/"+si.maxRetry+"):"+www.error + " Detail : "+www.text); 
			si.retryCount ++;
			StartCoroutine( ExecuteService(si) );
		}

		// otherwise it's a connection issue
		else
		{
			// problem trying to connect // TODO : we need to define in which case we display a generic "retry popup" and in which case we don't
			if(si.useRetryPopupSystem)
			{
				logService("SHOW RETRY POPUP");
				//loadingView.ShowError(""+www.error);
				//
				//loadingView.RETRY_CLICKED += OnUserRetry;
			}

			result.message = "Oops.. Connection Issue. Please wait a moment then retry...";//result.message + " " + www.error;
			serviceError(si, result);
		} 
	}

	void OnUserRetry()
	{
		//loadingView.hide();
		//loadingView.RETRY_CLICKED -= OnUserRetry;
		lastService.retryCount = 0;
		StartCoroutine( ExecuteService(lastService) );
	}
	
	void serviceError( ServiceInfo si, ServiceResult result )
	{
		logServiceWarning("===> Service failed ("+si.url+"):" + result.message);

		// unblock ui on service error
		BlockUI.Block (false);

		if( si.onError != null ) si.onError( result );

		// hide loading view
		if (loadingView != null)
			loadingView.hide ();

		// no more running
		isRunning = false;
	}

	void addFormField( WWWForm theForm, string paramName, string value )
	{	
		theForm.AddField (paramName, value);
		if(!DebugVo.DO_NOT_LOG_FORM_PARAMS) logService("--> '"+paramName+"'="+value);
	}

	// service infos are logged in color for ease
	static void logService( string text )
	{
		Debug.Log ("<color=yellow>" + text + "</color>");
	}
	
	static void logServiceWarning( string text )
	{
		Debug.LogWarning ("<color=red>" + text + "</color>");
	}



	/* EXAMPLE WITH HEADERS !
	
	 public WWWForm form = new WWWForm();
    public stringstring headers = form.headers;
    public byte[] rawData = form.data;
    public string url = "www.myurl.com";
    public WWW www = new WWW(url, rawData, headers);
    IEnumerator Example() {
        form.AddField("name", "value");
        headers["Authorization"] = "Basic " + System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes("username:password"));
        yield return www;
    }
    */

	
	/**
	 * -------------------------------------- INTERNAL SERVICE CLASS ----------------------------------------
	 */
	
	class ServiceInfo
	{
		static int serviceUID = 0; // increment each time we add a new service to generate uniq service id

		// props
		#pragma warning disable 0649
		public string tag;  // optinal service tag if needed
		#pragma warning restore 0649

		public Action<ServiceResult> onSuccess;
		public Action<ServiceResult> onError;
		public string url;
		public WWWForm form;
		public Dictionary<string, string> headers;
		public ServiceResult.ResultType resultType;
		public bool useRetryPopupSystem = false;
		public bool logResult = true; // log raw result from webservice

		public int maxRetry = 3;
		public int retryCount = 0;
		
		// constructor
		public ServiceInfo( string url, WWWForm form, Dictionary<string, string> headers, Action<ServiceResult> onSuccess, Action<ServiceResult> onError, ServiceResult.ResultType resultType = ServiceResult.ResultType.TEXT )
		{
			serviceUID ++;
			//id = serviceUID;
			this.onSuccess = onSuccess;
			this.onError = onError;
			this.url = url;
			this.headers = headers;
			this.form = form;
			this.resultType = resultType;
		}
	}


	/**
	 * -------------------------------------- CRYPTO ----------------------------------------
	 */

	public static string GetSHA1HashString(string unhashed)
	{
		logService("Generating SHA1 hash value from: \"" + unhashed + "\"");
		byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(unhashed);
		System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create();
		byte[] hash = sha.ComputeHash(inputBytes);
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		for (int i = 0; i < hash.Length; i++)
		{
			sb.Append(hash[i].ToString("x2"));
		}
		return sb.ToString();
	}
	
	public static string GetMD5HashString(string unhashed)
	{
		logService("Generating MD5 hash value from: \"" + unhashed + "\"");
		byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(unhashed);
		System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
		byte[] hash = md5.ComputeHash(inputBytes);
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		for (int i = 0; i < hash.Length; i++)
		{
			sb.Append(hash[i].ToString("x2"));
		}
		return sb.ToString();
	}

}


/**
 * -------------------------------------- PUBLIC SERVICE RESULT CLASS ----------------------------------------
 */

public class ServiceResult
{
	public enum ResultType
	{
		TEXT,
		XML,
		JSON,
		PLAYANDGOLD
	}

	public string message;
	public string raw;
	public bool success;
	public string httpStatus;
	public string errorCode;
	public Dictionary<string, object> content;
	
	// constructor
	public ServiceResult( string result, ResultType resultType )
	{
		// save raw result
		raw = ""+result;
		//logService ("==> raw result : " + result);


		// RESULT TYPE TEXT
		if (resultType == ResultType.TEXT) {
			success = true;
		}

		// RESULT TYPE PLAYANDGOLD
		/*
		else if (resultType == ResultType.PLAYANDGOLD) 
		{
			try 
			{
				// transform result in dictionnary
				Dictionary<string, object> dic = (Dictionary<string,object>) MiniJSON.Json.Deserialize( result );
				Dictionary<string, object> header = (Dictionary<string, object>) dic["header"];
				content = (Dictionary<string, object>) dic["body"];
				
				// get status and message
				httpStatus =  ""+ header["status"].ToString();

				if(httpStatus == "200") success = true;	
				else
				{
					// check for error code
					success = false;
					errorCode = ""+ content["code"].ToString();
				}
			} 
			catch (Exception ex) {				
				success = false;
				message = "parsing result error : " + ex.ToString() + ", see raw for more detail";	
			}
		}
		
		// RESULT TYPE JSON
		else 
		*/
		if(resultType == ResultType.JSON)
		{
			try 
			{
				// transform result in dictionnary
				Dictionary<string, object> dic = (Dictionary<string,object>) MiniJSON.Json.Deserialize( result );
				//Dictionary<string, object> datas = (Dictionary<string, object>) dic["data"];
				
				// get status and message
				string statusString =  dic["status"].ToString();
				message =  dic["message"].ToString();
				
				if(statusString == "1") 
				{
					success = true;	
					if(dic.ContainsKey("data")) content =  (Dictionary<string, object>)dic["data"];
				}
				else
				{
					success = false;
				}
			} 
			catch (Exception ex) {
				
				success = false;
				//message = "result parsing error: \nJSON error: " +MiniJSON.Json+ "\nException: " + ex.ToString();	
				message = "result parsing error: " + ex.ToString();	
			}
		}


		// RESULT TYPE XML
		else if(resultType == ResultType.XML)
		{
			try 
			{
				XmlDocument xmlDoc = new XmlDocument(); // xmlDoc is the new xml document.
				xmlDoc.LoadXml(result); // load the data.
				XmlNode resultNode = xmlDoc.SelectSingleNode("result");

				// get status and message
				string statusString =  resultNode.SelectSingleNode("status").InnerText; 
				message =  resultNode.SelectSingleNode("message").InnerText; 
				
				if(statusString == "1") 
				{
					success = true;	
					//if(dic.ContainsKey("content")) content =  (Dictionary<string, object>)dic["content"];
				}
				else
				{
					success = false;
				}
			} 
			catch (Exception ex) {
				
				success = false;
				message = "result parsing error : " + ex.ToString();	
			}
		}
		
	}
}






