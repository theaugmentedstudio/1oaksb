﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class HeaderView : ViewBehavior {

	/**------------------------------ PROPERTIES --------------------------------**/

	// states

	// inspector values
	public Button backButton;
	public Text roleLabel;


	/**------------------------------ VIEW IMPL --------------------------------**/
	
	protected override void OnEnableImpl ()
	{
		if ((int)AppData.sessionVo.sessionRole > 1) 
		{
			roleLabel.gameObject.SetActive(true);
			roleLabel.text = "Mode: "+AppData.sessionVo.sessionRole.ToString();
		} 
		else
			roleLabel.gameObject.SetActive (false);
	}

	protected override void OnDisableImpl ()
	{

	}


	/**------------------------------ Update BackButton --------------------------------**/

	public void UpdateBackButton( bool visibility )
	{
		backButton.interactable = false;
		backButton.gameObject.SetActive (visibility);
		Invoke ("reactivateButton", 1.0f);
	}
	void reactivateButton()
	{
		backButton.interactable = true;
	}


	/**------------------------------ Button Actions --------------------------------**/

	public void OnBackButtonHandler()
	{
		hide ();
		//GUIManager.Instance.GetView (GUIManager.Views.ScanView).SetActive (true);
		GUIManager.Instance.ShowViewAsMain(GUIManager.Views.ScanView);
	}
}
