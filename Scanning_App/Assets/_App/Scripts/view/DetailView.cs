﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class DetailView : ViewBehavior {

	/**------------------------------ PROPERTIES --------------------------------**/

	// states

	// inspector values
	public CanvasGroup orderBlock;

	// content
	public Text orderIdLabel;
	public Text dateLabel;
	public Text barcodeLabel;

	public Text firstnameLabel;
	public Text lastnameLabel;
	public Text phoneLabel;
	public Text adressLabel;
	public Text ticketLabel;

	// states
	public GameObject checkInState;
	public GameObject checkInCompleteState;
	public GameObject checkInInvalidState;

	public Button checkinButton;

	public GameObject checkoutContainer;
	public Button checkoutButton;

	public Text invalidTicketLabel;
	public Text errorLabel;


	// 


	/**------------------------------ VIEW IMPL --------------------------------**/
	
	protected override void OnEnableImpl ()
	{
		GAHelper.LogScreen ("CheckinView");

		// update detail
		orderIdLabel.text = "" + AppData.orderVo.orderID;
		dateLabel.text = "" + AppData.orderVo.eventDate;
		barcodeLabel.text = "" + AppData.orderVo.barcode;

		firstnameLabel.text = "" + AppData.orderVo.user.firstname;
		lastnameLabel.text = "" + AppData.orderVo.user.lastname;
		phoneLabel.text = (string.IsNullOrEmpty(AppData.orderVo.user.phone))? "Unknown" : AppData.orderVo.user.phone;
		adressLabel.text = (string.IsNullOrEmpty(AppData.orderVo.user.adress))? "Unknown" : AppData.orderVo.user.adress.Replace(",", "\n");// + "\n"+ AppData.orderVo.user.adress2 + "\n"+ AppData.orderVo.user.adress3; string.Replace(",", "\n");
		ticketLabel.text = "" + AppData.orderVo.productDescription; 
		if(!string.IsNullOrEmpty(AppData.orderVo.productVariation)) ticketLabel.text += "\n" + AppData.orderVo.productVariation;

		// always hide checkout button
		checkoutContainer.SetActive(false);

		// hide checking complete state
		checkInCompleteState.SetActive (false);

		// hide error label
		errorLabel.gameObject.SetActive (false);

		// check if ticket is already used
		if (AppData.orderVo.used) {
			invalidTicketHandler ("This ticket has already been used\non " + AppData.orderVo.checkinDate);
			GAHelper.LogScreen ("CheckinView/alreadyUsed");

			// if event is not yet complete, we can still make a checkout in admin mode
			if( AppData.sessionVo.sessionRole == SESSION_ROLE.ADMIN && !AppData.orderVo.isEventAlreadyOver )
			{
				checkoutContainer.SetActive(true);
				checkoutButton.interactable = true;
			}


		// check if event is passed
		} else if ( AppData.orderVo.isEventAlreadyOver && !DebugVo.DO_NOT_CHECK_DATE ) {
			invalidTicketHandler ("This ticket was only valid\non " + AppData.orderVo.eventDate );
			GAHelper.LogScreen ("CheckinView/wrongDate");
		

		// if hostess, we can only checkin a ticket during the event
		} else if (AppData.sessionVo.sessionRole == SESSION_ROLE.HOSTESS && !AppData.orderVo.isEventNow && !DebugVo.DO_NOT_CHECK_DATE ) {
			invalidTicketHandler ("This ticket will only be valid\non " + AppData.orderVo.eventDate );
			GAHelper.LogScreen ("CheckinView/wrongDate");
		

		// otherwise we can make a checkin
		} else
		{
			invalidTicketLabel.gameObject.SetActive (false);
			orderBlock.alpha = 1;
			orderBlock.transform.localScale = Vector3.one;
			checkinButton.interactable = true;
			checkInState.SetActive (true);
			checkInInvalidState.SetActive(false);
		}

		// finaly activate or not the checkout button
		checkoutButton.gameObject.SetActive( AppData.sessionVo.sessionRole == SESSION_ROLE.ADMIN );
	}

	void invalidTicketHandler( string message )
	{
		// state
		checkInState.SetActive (false);
		checkInInvalidState.SetActive(true);

		// order block position
		orderBlock.alpha = 0.7f;
		orderBlock.transform.localScale = Vector3.one * 0.8f;

		// label
		invalidTicketLabel.gameObject.SetActive (true);
		invalidTicketLabel.text = message;
		checkinButton.interactable = false;
	}

	
	protected override void OnDisableImpl ()
	{

	}


	/**------------------------------ PUBLIC METHODS --------------------------------**/


	/**------------------------------ Checkin --------------------------------**/
	
	void doCheckin()
	{
		// check code (it should be ok at this step but we never know...)
		if(AppData.orderVo.barcode == null)
		{
			Debug.LogWarning("DetailView.doChecking : code is invalid : "+AppData.orderVo.barcode);
			showError("Code is invalid, unable to Check-in.");
			return;
		}
		
		BlockUI.Block (true);
		checkinButton.interactable = false;
		ServiceManager.Instance.CheckIN ( AppData.orderVo.barcode, onCheckinSuccess, onCheckinFailed );
	}
	void onCheckinSuccess(ServiceResult result)
	{
		BlockUI.Block (false);
		if (result.success) 
		{
			// success
			checkInCompleteState.SetActive(true);
			checkInState.SetActive(false);
			
			//
			GAHelper.LogScreen ("CheckinView/checkInSuccess");
			
			// order block position
			TweenUtil.ScaleTo(orderBlock.gameObject, orderBlock.transform.localScale, Vector3.one*0.8f,0.5f,0);
			orderBlock.alpha = 0.7f;
		} 
		else 
		{
			showError(result.message);
		}
	}
	void onCheckinFailed(ServiceResult result)
	{
		showError (result.message);
		GAHelper.LogScreen ("CheckinView/checkInFailed");
		//GAHelper.LogEvent ("service", "checkin", "checkinFailed");
	}
	void showError( string errorMessage)
	{
		// display message
		errorLabel.text = errorMessage;
		errorLabel.gameObject.SetActive (true);
		
		// reactivate button
		BlockUI.Block (false);
		checkinButton.interactable = true;
	}



	/**------------------------------ checkout (admin only) --------------------------------**/
	
	void doAdminCheckout()
	{
		// check code (it should be ok at this step but we never know...)
		if(AppData.orderVo.barcode == null)
		{
			Debug.LogWarning("DetailView.doCheckout : code is invalid : "+AppData.orderVo.barcode);
			showError("Code is invalid, unable to Check-Out.");
			return;
		}
		
		BlockUI.Block (true);
		checkoutButton.interactable = false;
		ServiceManager.Instance.CheckOUT ( AppData.orderVo.barcode, doAdminCheckout_success, doAdminCheckout_failed );
	}
	void doAdminCheckout_success(ServiceResult result)
	{
		BlockUI.Block (false);
		if (result.success) 
		{
			GAHelper.LogScreen ("CheckinView/checkOutSuccess");

			
			// check if result is correct
			if(result.content == null)
			{
				showCheckoutError("This ticket is not valid");
				return;
			}
			
			// fill order
			AppData.orderVo.fill (result.content);
			
			// relaunch view
			GUIManager.Instance.ShowViewAsMain(GUIManager.Views.DetailView, true);
		} 
		else 
		{
			showCheckoutError(result.message);
		}
	}
	void doAdminCheckout_failed(ServiceResult result)
	{
		showCheckoutError (result.message);
	}
	void showCheckoutError( string errorMessage)
	{
		GAHelper.LogScreen ("CheckinView/checkOutFailed");

		// display message
		invalidTicketLabel.text = errorMessage;

		// reactivate button
		BlockUI.Block (false);
		checkoutButton.interactable = true;
	}





	/**------------------------------ Button Actions --------------------------------**/

	public void OnBackButtonHandler()
	{
		hide ();
		//GUIManager.Instance.GetView (GUIManager.Views.ScanView).SetActive (true);
		GUIManager.Instance.ShowViewAsMain(GUIManager.Views.ScanView);
	}


	public void OnCheckinButtonHandler()
	{
		doCheckin ();
	}

	public void OnCheckoutButtonHandler()
	{
		doAdminCheckout ();
	}


	public void OnHomeButtonHandler()
	{
		hide ();
		//GUIManager.Instance.GetView (GUIManager.Views.HomeView).SetActive (true);
		GUIManager.Instance.ShowViewAsMain(GUIManager.Views.HomeView);
	}






}
