﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class HomeView : ViewBehavior {

	// inspector
	public InputField codeInput;
	public Text errorLabel;
	public Button codeInputButton;


	/**
 	 * -------------------------------------- view implementation ----------------------------------------
	 */

	protected override void initImpl ()
	{
	}

	protected override void OnEnableImpl ()
	{
		GAHelper.LogScreen ("HomeView");

		// reset
		errorLabel.gameObject.SetActive (false);
		codeInput.text = "";
		codeInputButton.interactable = true;
	}

	protected override void OnDisableImpl ()
	{
	}




	/**
 	 * -------------------------------------- BUTTON ACTIONS ----------------------------------------
	 */

	public void OnScanTicketButtonHandler()
	{
		hide ();
		//GUIManager.Instance.GetView (GUIManager.Views.ScanView).SetActive (true);
		GUIManager.Instance.ShowViewAsMain(GUIManager.Views.ScanView);
	}


	public void OnEnterTicketIDButtonHandler()
	{
		// desactivate button
		codeInputButton.interactable = false;
		BlockUI.Block (true);
		string code = codeInput.text;

		// validate code first
		if (codeInput.text == "") {
			showError("Invalid code");
			return;
		}

		// call service
		ServiceManager.Instance.GetOrder ( code, onGetOrderSuccess, onGetOrderFail );
	}
	void onGetOrderSuccess (ServiceResult result)
	{
		// reset block ui
		BlockUI.Block (false);

		if (result.success) {

			// check if result is correct
			if(result.content == null)
			{
				showError("This ticket is not valid");
				return;
			}

			// fill order
			AppData.orderVo.fill (result.content);

			// hide
			hide ();

			// go to detail view
			//GUIManager.Instance.GetView (GUIManager.Views.DetailView).SetActive (true);
			GUIManager.Instance.ShowViewAsMain(GUIManager.Views.DetailView);
		} 
		else 
		{
			showError(result.message);
		}
	}
	void onGetOrderFail (ServiceResult result)
	{
		showError (result.message);
	}


	void showError( string errorMessage)
	{
		// display message
		errorLabel.text = errorMessage;
		errorLabel.gameObject.SetActive (true);

		// reactivate button
		BlockUI.Block (false);
		codeInputButton.interactable = true;
	}





	/**
 	 * -------------------------------------- ANIMATION ----------------------------------------
	 */


	/*
	public void showFromLastPage()
	{
		showDuration = 1;
		showAlphaFrom = 0;
		showStartMenuDelay = 4.0f;
		SetActive (true);
	}
	*/
	
	/*
	void showMenuElements()
	{
		RectTransform item;

		for (int i = 0; i < menuItems.Length; i++)
		{
			item = menuItems[i];
			animateItem(item, new Vector2(itemPos[i].x, itemPos[i].y), 1);
		}
	}
	*/
	
	/*
	void animateItem( RectTransform item, Vector3 newPos, float newScale)
	{
		float duration = 0.5f;
		DOTween.To(()=> item.anchoredPosition, x=> item.anchoredPosition = x, newPos, duration).SetDelay(0).SetEase(Ease.OutCubic);
		DOTween.To(()=> item.localScale, x=> item.localScale = x, Vector3.one*newScale , duration).SetDelay(0).SetEase(Ease.OutCubic);
	}
	*/


}
