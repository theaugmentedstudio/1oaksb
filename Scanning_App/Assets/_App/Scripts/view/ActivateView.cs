﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ActivateView : ViewBehavior {

	/**------------------------------ PROPERTIES --------------------------------**/

	// states

	// inspector values
	public InputField codeInput;
	public Text errorLabel;


	/**------------------------------ VIEW IMPL --------------------------------**/
	
	protected override void OnEnableImpl ()
	{
		GAHelper.LogScreen ("ActivateView");

		// reset
		errorLabel.gameObject.SetActive (false);
		codeInput.shouldHideMobileInput = false;
		codeInput.text = "";
	}
	
	protected override void OnDisableImpl ()
	{

	}


	/**------------------------------ PUBLIC METHODS --------------------------------**/



	/**------------------------------ Button Actions --------------------------------**/

	public void OnContinueButtonHandler()
	{
		string codeHash = ServiceManager.GetSHA1HashString( codeInput.text );
		Debug.Log ("code : " + codeHash);

		// Check activation key
		switch (codeHash) 
		{
			case SessionVo.access_admin : 
			AppData.sessionVo.sessionRole = SESSION_ROLE.ADMIN;
			break;
			case SessionVo.access_checkin : 
            AppData.sessionVo.sessionRole = SESSION_ROLE.CHECKIN;
			break;
			case SessionVo.access_hostess : 
			AppData.sessionVo.sessionRole = SESSION_ROLE.HOSTESS;
			break;

			default:
			AppData.sessionVo.sessionRole = SESSION_ROLE.NONE;
			break;
		}

		// block app if no valid session role
		if( AppData.sessionVo.sessionRole == SESSION_ROLE.NONE) {
			errorLabel.gameObject.SetActive(true);
		}

		// valid
		else 
		{
			Debug.Log("ActivateView.sessionRole Set to : "+AppData.sessionVo.sessionRole.ToString());

			PlayerPrefs.SetInt("activated", (int)AppData.sessionVo.sessionRole);
			PlayerPrefs.Save();

			// hide
			hide();

			// show home
			//GUIManager.Instance.GetView(GUIManager.Views.HomeView).SetActive(true);
			GUIManager.Instance.ShowViewAsMain(GUIManager.Views.HomeView);
		}
	}


	/*
	public void HomeButtonClicked()
	{
		if (helpIsOpen)
			ExitHelp ();

		// show dashboard
		DashboardView dashboard = GUIManager.Instance.GetView (GUIManager.Views.DashboardView) as DashboardView;
		if (GUIManager.Instance.currentView != dashboard && GUIManager.Instance.currentView != null) {
			// hide current view
			GUIManager.Instance.currentView.hide();
			dashboard.SetActive (true);
			GUIManager.Instance.currentView = dashboard;
		}

		// as there is only one way to go out of experience (home button) we can check here to close sub views
		// be sure to always close menu and detailview if we close
		ViewBehavior menu = GUIManager.Instance.GetView (GUIManager.Views.MenuView);
		if (menu.gameObject.activeSelf)
			menu.hide ();
		
		ViewBehavior detail = GUIManager.Instance.GetView (GUIManager.Views.DetailView);
		if (detail.gameObject.activeSelf)
			detail.hide ();


		// show modal if not active
		GUIManager.Instance.UpdateModal ();

	}

	public void HelpButtonClick()
	{
		helpIsOpen = true;
		// hide current view if there is one
		if (GUIManager.Instance.currentView != null) 
		{
			GUIManager.Instance.currentView.hide();
		}

		// show modal if not active
		ViewBehavior modal = GUIManager.Instance.GetView (GUIManager.Views.ModalView);
		if (!modal.gameObject.activeSelf)
			modal.SetActive (true);

		// show menu if not active
		//hideMenuOnClose = false;
		MenuView menu = GUIManager.Instance.GetView (GUIManager.Views.MenuView) as MenuView;
		if (!menu.gameObject.activeSelf) {
			menu.SetActive (true);
			//hideMenuOnClose = true;
		}
		// be sure menu is not alpha0 during tracking
		menu.TrackingHide (false);

		
		// show help
		GUIManager.Instance.GetView (GUIManager.Views.HelppView).SetActive (true);
	}

	public void ExitHelp()
	{
		helpIsOpen = false;
		// hide help
		GUIManager.Instance.GetView (GUIManager.Views.HelppView).hide ();

		// check to reactive old current view
		if (GUIManager.Instance.currentView != null)
			GUIManager.Instance.currentView.SetActive(true);
		else
			GUIManager.Instance.ShowViewAsMain (GUIManager.Views.DashboardView);

		// always hide menu if open as we reset scene when quitting view
		if(GUIManager.Instance.GetView (GUIManager.Views.MenuView).gameObject.activeSelf)
			GUIManager.Instance.GetView (GUIManager.Views.MenuView).hide();



		// hide modal if needed
		GUIManager.Instance.UpdateModal ();

	}
	*/


}
