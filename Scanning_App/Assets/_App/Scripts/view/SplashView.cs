﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SplashView : ViewBehavior {
	
	// inspector
	
	
	/**
 	 * -------------------------------------- view implementation ----------------------------------------
	 */
	
	protected override void initImpl ()
	{
	}
	
	protected override void OnEnableImpl ()
	{
		//Invoke ("ShowDash", 1.0f);
	}
	
	protected override void OnDisableImpl ()
	{
	}
	
	
	
	
	/**
 	 * -------------------------------------- ANIMATION ----------------------------------------
	 */
	
	/*
	public void showFromLastPage()
	{
		showDuration = 1;
		showAlphaFrom = 0;
		showStartMenuDelay = 4.0f;
		SetActive (true);
	}
	*/
	
	/*
	void showMenuElements()
	{
		RectTransform item;

		for (int i = 0; i < menuItems.Length; i++)
		{
			item = menuItems[i];
			animateItem(item, new Vector2(itemPos[i].x, itemPos[i].y), 1);
		}
	}
	*/
	
	void animateItem( RectTransform item, Vector3 newPos, float newScale)
	{
		float duration = 0.5f;
		DOTween.To(()=> item.anchoredPosition, x=> item.anchoredPosition = x, newPos, duration).SetDelay(0).SetEase(Ease.OutCubic);
		DOTween.To(()=> item.localScale, x=> item.localScale = x, Vector3.one*newScale , duration).SetDelay(0).SetEase(Ease.OutCubic);
	}
	
	
	/**
 	 * -------------------------------------- BUTTON ACTIONS ----------------------------------------
	 */
	
	
	public void ShowDash()
	{
		hide ();
		//ViewBehavior nextView = GUIManager.Instance.GetView (GUIManager.Views.DashboardView); 
		//nextView.SetActive (true);
	}
	
	
}