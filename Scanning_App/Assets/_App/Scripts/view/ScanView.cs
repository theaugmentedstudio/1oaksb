﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using Lean;
using Vuforia;

public class ScanView : ViewBehavior {

	/**------------------------------ PROPERTIES --------------------------------**/

	// states

	// inspector values
	public Text errorLabel;
	public GameObject scannerPrefab;
	public ViewBehavior scanEffect;
    public Text codeFoundLabel;
	
	VuforiaScanner barcodeScanner;


	/**------------------------------ VIEW IMPL --------------------------------**/
	
	protected override void OnEnableImpl ()
	{
		GAHelper.LogScreen ("ScanView");

		// do not allow back action when enabling, allow only after scanning is resumed 
		//GUIManager.Instance.allowBackAction = false;

		// remove error label
		errorLabel.gameObject.SetActive (false);
		scanEffect.gameObject.SetActive (false);
        codeFoundLabel.gameObject.SetActive(false);

		// listen
		VuforiaScanner.SCAN_SUCCESS -= onScanResult;
		VuforiaScanner.SCAN_SUCCESS += onScanResult;

		Lean.LeanTouch.OnFingerTap -= refreshCameraFocus;
		Lean.LeanTouch.OnFingerTap += refreshCameraFocus;

		// start inactivity timer
		startInactivityTimer ();

		//delay when activating scanner for smooth transition
		Invoke ("resumeScanning", 0.5f);
	}
	
	protected override void OnDisableImpl ()
	{
		Lean.LeanTouch.OnFingerTap -= refreshCameraFocus;
		VuforiaScanner.SCAN_SUCCESS -= onScanResult;

		// stop inactivity timer
		stopInactivityTimer ();

		// stop scan feature
		barcodeScanner.PauseScan ();

		// be sure to kill possible invoke to resume scanning
		CancelInvoke ("resumeScanning");
		        
		//delay when desactivating scanner video for smooth transition
		Invoke ("stopScanning", 0.5f);

        // remove fingertap
        LeanTouch.OnFingerTap -= onFingerTap;

        stopFlash();
	}


	void stopScanning()
	{
		barcodeScanner.Dispose ();

		// release render texture to avoid vuforia issue
		/*
		if (RenderTexture.active != null) {
			RenderTexture.active.Release ();
			RenderTexture.active = null;
			if(RenderTexture.active != null)
				DestroyImmediate(RenderTexture.active);
		}
		*/

		// destroy full scanner system
		Destroy (barcodeScanner.gameObject);
		//barcodeScanner.StopScanning ();
	}
	void resumeScanning()
	{
		if (barcodeScanner == null)
			barcodeScanner = Instantiate (scannerPrefab).GetComponent<VuforiaScanner>();
		
		barcodeScanner.ResumeScanning ();

        // add lean touch update
        LeanTouch.OnFingerTap -= onFingerTap;
        LeanTouch.OnFingerTap += onFingerTap;
	}
	void refreshCameraFocus( Lean.LeanFinger finger)
	{
		if(barcodeScanner != null)
			barcodeScanner.RefreshCameraFocus ();
	}

	/**------------------------------ inactivity timer --------------------------------**/
	
	void startInactivityTimer()
	{
		stopInactivityTimer ();
		Invoke ("BackAfterInactivity", 10f);
	}
	void stopInactivityTimer()
	{
		CancelInvoke ( "BackAfterInactivity" );
	}
	void BackAfterInactivity()
	{
		stopInactivityTimer();
		barcodeScanner.PauseScan ();
		hide ();
		GUIManager.Instance.ShowViewAsMain(GUIManager.Views.HomeView);
	}



    // trigger auto focus

    void onFingerTap( LeanFinger finger )
    {
        Debug.Log("onFingerTap");
        startInactivityTimer(); // each time we tap the screen we keep the inactivity timer running
        if (CameraDevice.Instance != null)
        {
            //Debug.Log("Focus mode is : "+CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO)
            bool focusTrigger = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
            Debug.Log("Camera Focus trigger success : " + focusTrigger);
            CancelInvoke("stopFlash");
            CameraDevice.Instance.SetFlashTorchMode(true);
            Invoke("stopFlash", 2f);

        }
    }
    void stopFlash()
    {
        if (CameraDevice.Instance != null)
        {
            Debug.Log("Stop camera flash");
            CameraDevice.Instance.SetFlashTorchMode(false);
        }
    }



	/**------------------------------ GET ORDER --------------------------------**/

	void onScanResult( string barcodeResult )
	{
		// be sure to remove error when a new scanning occurs
		errorLabel.gameObject.SetActive (false);

		// show scan effect
		scanEffect.SetActive (true);
		scanEffect.hide ();

		//
		stopInactivityTimer ();
		GetOrder (barcodeResult);
	}


	void GetOrder( string code )
	{
		// desactivate button
		BlockUI.Block (true);

		// TODO : pre validate code on app? (format?)

        // display code found label
        codeFoundLabel.gameObject.SetActive(true);

		// call service
		ServiceManager.Instance.GetOrder ( code, onGetOrderSuccess, onGetOrderFail );
	}
	void onGetOrderSuccess (ServiceResult result)
	{
		// reset block ui
		BlockUI.Block (false);
		
		if (result.success) {

			// check if result is correct
			if(result.content == null)
			{
				showError("This ticket is not valid");
				return;
			}

			// fill order
			AppData.orderVo.fill (result.content);
			
			// hide
			hide ();
			
			// go to detail view
			//GUIManager.Instance.GetView (GUIManager.Views.DetailView).SetActive (true);
			GUIManager.Instance.ShowViewAsMain(GUIManager.Views.DetailView);
		} 
		else 
		{
			showError(result.message);
		}
	}
	void onGetOrderFail (ServiceResult result)
	{
		showError (result.message);
	}
	void showError( string errorMessage)
	{
		// display message
		errorLabel.text = errorMessage;
		errorLabel.gameObject.SetActive (true);
		
		// reactivate button
		BlockUI.Block (false);

		// restart scanning after 2 seconds
		Invoke ("resumeScanning", 2.0f);

		GAHelper.LogScreen ("ScanView/error");
		//GAHelper.LogEvent ("User", "Scanning", "ScanError");

        codeFoundLabel.gameObject.SetActive(false);

		// restart inactivity timer
		startInactivityTimer ();
	}
}
