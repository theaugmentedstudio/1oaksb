﻿using UnityEngine;
using System;
using System.Collections;

using Vuforia;

using System.Threading;

using ZXing;
using ZXing.QrCode;
using ZXing.Common;


[AddComponentMenu("System/VuforiaScanner")]
public class VuforiaScanner : MonoBehaviour
{    
	// events
	public delegate void ScanSuccessDelegate( string code );
	public static ScanSuccessDelegate SCAN_SUCCESS;


	// inspector
	public VuforiaBehaviour vuforiaBehavior;


	//public Renderer imagePreview;
	//public UnityEngine.UI.Text BarCodeLabel; 
	bool pauseScan;

	// private
	private bool cameraInitialized;	
	private BarcodeReader barCodeReader;



	/*---------------------------------------- MONO -------------------------------------------------*/

	void Awake()
	{        
		barCodeReader = new BarcodeReader();
		barCodeReader.ResultFound += barcodeResultFound;
	}

	void OnDisable()
	{
		//Debug.LogWarning("VuforiaScanner should never been stopped as, on android, this will lead to failure in next initialization");
	}



	/*-------------------- STOP RESUME QR scanning if it was paused --------------------*/
	
	public void ResumeScanning()
	{
		Debug.Log ("VuforiaScanner.ResumeScanning");

		// init if not yet done
		if (!cameraInitialized)
			StartCoroutine (InitializeCamera ());

		// else simply listen ot update
		else {
			//if(!vuforiaBehavior.enabled) vuforiaBehavior.enabled = true;
			CameraDevice.Instance.Start ();
			pauseScan = false;
		}
	}

	/*
	public void StopScanning()
	{
		Debug.Log ("VuforiaScanner.StopScanning");

		// stop vuforia
		CameraDevice.Instance.Stop ();
		//vuforiaBehavior.enabled = false;

		pauseScan = true;
	}
	*/


	public void PauseScan()
	{
		pauseScan = true;
	}


	public void Dispose()
	{
		vuforiaBehavior.enabled = false;
	}


	public void RefreshCameraFocus()
	{
		Debug.Log ("VuforiaScanner.RefreshCameraFocus");
		/*
		var isAutoFocus = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		if (!isAutoFocus)
		{
			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		}
		*/
		if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO))
			Debug.Log(" -->Camera focus trigger success");
	}



	/*---------------------------------------- INITIALIZE -------------------------------------------------*/

	
	private IEnumerator InitializeCamera()
	{
		Debug.Log ("VuforiaScanner.InitializeCamera");

		// activate vuforia camera
		vuforiaBehavior.enabled = true;

		// Waiting a little seem to avoid the Vuforia's crashes.
		yield return new WaitForSeconds(2.0f);

		// reset camera
		//resetCamera ();

		// wait camera reset
		//yield return new WaitForSeconds(0.5f);

		// set camera frame format

#if UNITY_EDITOR
		var isFrameFormatSet = CameraDevice.Instance.SetFrameFormat(Image.PIXEL_FORMAT.GRAYSCALE, true);
#else
		var isFrameFormatSet = CameraDevice.Instance.SetFrameFormat(Image.PIXEL_FORMAT.RGB888, true);
#endif
		Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet));
		
		// Force autofocus.
		var isAutoFocus = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		if (!isAutoFocus)
		{
			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		}
		Debug.Log(String.Format("AutoFocus : {0}", isAutoFocus));
		cameraInitialized = true;
		pauseScan = false;
	}

	/**
	 * as it seems to be a real issue to desactivate then reactivate camera on android device, we need to completely kill camera device and reset
	 */
	void resetCamera()
	{
		if (CameraDevice.Instance != null) {
			//CameraDevice.CameraDirection cd = CameraDevice.Instance.GetCameraDirection();
			CameraDevice.CameraDirection cd = CameraDevice.CameraDirection.CAMERA_DEFAULT;
			
			// stop current camera
			CameraDevice.Instance.Stop();
			CameraDevice.Instance.Deinit();
			VuforiaRenderer.Instance.ClearVideoBackgroundConfig();
			
			// switch camera
			CameraDevice.Instance.Init(cd);
			
			// reset camera
			CameraDevice.Instance.SelectVideoMode(CameraDevice.CameraDeviceMode.MODE_DEFAULT);
            CameraDevice.Instance.SetFlashTorchMode(true);
            CameraDevice.Instance.Start();
		}
	}




	/*-------------------- update --------------------*/

	//Texture2D newTex;
	int frameCpt = 0;
	int scanEachFrame = 25; // for perfs we only check image each xx frames.
	void Update()
	{
		frameCpt ++;
		if ( cameraInitialized && !pauseScan && frameCpt%scanEachFrame == 0 )
		{
			try
			{
#if UNITY_EDITOR
				Image cameraFeed = CameraDevice.Instance.GetCameraImage(Image.PIXEL_FORMAT.GRAYSCALE);//RGBA8888);
#else
				Image cameraFeed = CameraDevice.Instance.GetCameraImage(Image.PIXEL_FORMAT.RGB888);//Image.PIXEL_FORMAT.RGB888);
#endif
				if (cameraFeed == null)
				{
					Debug.Log("Camera feed is null");
					return;
				}

				/*
				// create text
				if(!newTex){
					newTex = new Texture2D(cameraFeed.Width, cameraFeed.Height,TextureFormat.RGB24,false);
					newTex.Apply();
					imagePreview.material.mainTexture = newTex;
				}

				// load bytes
				//newTex.mip
				//newTex.LoadRawTextureData(cameraFeed.Pixels);
				cameraFeed.CopyToTexture(newTex);
				*/



#if UNITY_EDITOR
				Result data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight, RGBLuminanceSource.BitmapFormat.Gray8); //

#else
				Result data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight, RGBLuminanceSource.BitmapFormat.RGB24);//RGBLuminanceSource.BitmapFormat.RGB24);
#endif
				/*
				if (data != null)
				{
					// QRCode detected.
					Debug.Log("BarcodeScanner.Update : scan data found " + data.Text);
					if(SCAN_SUCCESS != null)
						SCAN_SUCCESS(data.Text);

					// once codbar is detected, we pause the scanning
					pauseScan = true;

					//BarCodeLabel.text = ""+data.Text;
					//BarCodeLabel.color = Color.green;
				}
				else
				{
					/*
					Debug.Log("No QR code detected !");
					string s = "  size: " + cameraFeed.Width + "x" + cameraFeed.Height + "\n";
					s += "  bufferSize: " + cameraFeed.BufferWidth + "x" + cameraFeed.BufferHeight + "\n";
					s += "  stride: " + cameraFeed.Stride;
					Debug.Log(s);
					* /
				}
			*/
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}
	}    

	void barcodeResultFound( Result barcodeResult )
	{
		Debug.Log("BarcodeScanner.barcodeResultFound ! : scan data found " + barcodeResult.Text);
		if(SCAN_SUCCESS != null)
			SCAN_SUCCESS(barcodeResult.Text);
		
		// once codbar is detected, we pause the scanning
		pauseScan = true;
	}
}