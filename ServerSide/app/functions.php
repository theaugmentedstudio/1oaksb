<?php

require "app.php";


/**
* -------------------------------------- Get ticket information  ----------------------------------------
*/

function GetOrderInfos($code_string)
{

	// get order information
	$query = "SELECT * FROM ticketCheck WHERE ID='".$code_string."'";
	$result = sendQuery($query);

	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
	    $rows[] = $r;
	}

	if(count($rows)==0)
	{
		$result = null;
	}
	else
	{
		$result = $rows[0];

		// check on date 
		$eventDate = $result["date_event"];
		$startEventDate = date("Y-m-d H:m:s", strtotime($eventDate));  // from day start midnight
		$endEventDate = date("Y-m-d H:m:s", strtotime($eventDate." +29 hours")); // to next day 5:00
		$now = date("Y-m-d H:m:s");

		/*
		echo "(eventDate) : ".$eventDate."\n";
		echo "(startEventDate) : ".$startEventDate."\n";
		echo "(endEventDate) : ".$endEventDate."\n";
		echo "(today) : ".$today."\n";
		*/
		
		//if($startEventDate < $today < $endEventDate)
		/*$today = gmdate("Y-m-d H:m:s");
		echo "(today) : ".$today."\n";
		*/
		//$todayMinus12h = date("Y-m-d", strtotime('-12 hours')); // minus 12 because event will be from 12 to 12
		//echo "(today - 10h) : ".$todayMinus12h."\n";
		
		// set date check ok at first
		$dateCheck = "ok";

		// check if event is over
		if( $endEventDate < $now ){
			$dateCheck = "past";
		} 

		// check if event is not yet done
		else if ( $startEventDate > $now ){
			$dateCheck = "later";
		}
		
		//$result["wrongDate"] = ($startEventDate <= $now && $endEventDate >= $now )?"0":"1";
		$result["dateCheck"] = $dateCheck;
	}

	// return result object
	return $result;
}



/**
* -------------------------------------- Check-in ----------------------------------------
*/

function CheckIN($code_string)
{
	// save data from post values
	$date_checkin = date("Y-m-d H:i:s");
	$query = "UPDATE ticketCheck SET used=1, date_checkin='".$date_checkin."' WHERE ID='".$code_string."';";
	$result = sendQuery($query);

	global $sqlLink;
	if(mysqli_affected_rows($sqlLink) > 0)
	{
		return "true";
	}
	else
	{
		return "false";
	}
}


/**
* -------------------------------------- Check-Out ----------------------------------------
*/

function CheckOUT($code_string)
{
	// checkout
	$query = "UPDATE ticketCheck SET used=0, date_checkin=NULL WHERE ID='".$code_string."';";
	$result = sendQuery($query);

	// check if success
	global $sqlLink;
	if(mysqli_affected_rows($sqlLink) > 0)
	{
		$result = GetOrderInfos($code_string);
		return $result;
	}
	else
	{
		return null;
	}
}


?>