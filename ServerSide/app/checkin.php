<?php

require "functions.php";

/**
* -------------------------------------- PARAMS ----------------------------------------
*/


if(!isset($_POST["code"])) notifyError("No code", 0);


// retrieve user values values
$code = $_POST["code"];
//$code ="5693f5dbdb2a1/1429/15";

/**
* -------------------------------------- make checkin  ----------------------------------------
*/

// check in
$success = CheckIN($code);

// close sql
closeSql();

// print result
if($success == "true"){
	printResult(1, "Checkin completed for code:".$code, null);
}
else{
	printResult(0, "Checkin error for code:".$code, null);
}



?>