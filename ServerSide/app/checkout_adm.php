<?php

require "functions.php";

/**
* -------------------------------------- PARAMS ----------------------------------------
*/


if(!isset($_POST["code"])) notifyError("co001", 0); // no code
if(!isset($_POST["token"])) notifyError("coE002", 0); // no token

// retrieve user values values
$code = $_POST["code"];
$token = $_POST["token"];

$validToken = sha1($token_prefix.$code.$token_sufix);
if($token != $validToken){
	 notifyError("co003", 0); // invalid token
}


/**
* -------------------------------------- make checkout  ----------------------------------------
*/

// check in
$result = CheckOUT($code);

// close sql
closeSql();

// print result
printResult(1, "success", $result);


?>