<?php 

ini_set('default_charset', 'UTF-8');
error_reporting(E_ALL);
ini_set("display_errors", "On");

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
//date_default_timezone_set('Etc/UTC');
date_default_timezone_set('America/Los_Angeles');


/**
* -------------------------------------- CONFIG  ----------------------------------------
*/

$isDev = false;
$ip = $_SERVER["REMOTE_ADDR"];			// Server IP

// Token for secure communication (check out)
$token_prefix="1oaksb";
$token_sufix="2017";


// DEV VARIABLES
if($isDev)
{
	$app_name = "1oaksb.com (DEV)";			// name used in webservice infos, error mails	
	$admin_mail = "anthodb@gmail.com";		// adress mail to send error infos
	$root = "http://localhost:8888/1oaksb/"; // base url

	// SQL (LOCALHOST)
	$sql_host = "localhost"; 
	$sql_user = "root";
	$sql_pass = "root";
	$sql_db = "anthodatabase";

	// SMTP (GMAIL)
	$smtp_host = 'smtp.gmail.com'; // (GMAIL.com);//Set the hostname of the mail server
	$smtp_port = 587;//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$smtp_user = "anthodb.mailer@gmail.com"; // (GMAIL.com);//Username to use for SMTP authentication - use full email address for gmail
	$smtp_pass = "CuUnWGQCewbe9j2Z"; // (GMAIL.com);//Password to use for SMTP authentication
	
}
// PROD VARIABLES
else
{
	$app_name = "1oaksb.com";			// name used in webservice infos, error mails	
	$admin_mail = "anthodb@gmail.com";		// adress mail to send error infos
	$root = "https://www.1oaksb.com/scan_api/"; //"http://p4660.phpnet.org/project/glen/app/"; //$root = "http://
	
	// SQL 
	$sql_host = "cl1-sql22"; 
	$sql_user = "p3348_6";
	$sql_pass = "Jzir0EFiNHbJ";
	$sql_db = "p3348_6";

	// SMTP (POSTMARK)
	$smtp_host =  'smtp.postmarkapp.com' ; //(POSTMARK.com)  //Set the hostname of the mail server
	$smtp_port = 587;//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$smtp_user = "d19e9008-f05a-466b-b0c9-20f1e9a02e6e"; //Username to use for SMTP authentication - use full email address for gmail
	$smtp_pass = "d19e9008-f05a-466b-b0c9-20f1e9a02e6e"; //Password to use for SMTP authentication
}



// response
$status = 1;							// status of current service ( 0=not ok or 1=ok )
$message = "";							// error message of current service
$data = "";								// additional data can be passed to result (for example : <lang>fr</lang>)



/**
* -------------------------------------- GLOBAL FUNCTIONS  ----------------------------------------
*/


/**
 * DEFAULT ERROR HANDLER
*/
function notifyAndSendError($errnum, $errstr = "")
{
	// send an email if there was an error in a script
	global $admin_mail, $app_name, $ip;
	$subject = $app_name." PHP Script error (IP:".$ip.")";
	$body = $errstr + " -- " + $errnum;
	$headers = "";
	if(!mail($admin_mail, $subject, $body, $headers)) { 
		notifyError("Error while sending mail to admin");
	}

	// print
	notifyError($errstr,$errnum);
}

/**
 * SEND SQL QUERY
*/
function sendQuery($queryString)
{
	global $sqlLink;
  	$result = mysqli_query($sqlLink, $queryString);
	if(mysqli_error($sqlLink)){
		notifyAndSendError(0, mysqli_error($sqlLink));
	}
	return $result;
}

/**
 * SEND SQL QUERY
*/
function sendMultiQuery($multiQueryString)
{
	global $sqlLink;
  	$result = mysqli_multi_query($sqlLink, $multiQueryString);
	if(mysqli_error($sqlLink)){
		notifyAndSendError(0, mysqli_error($sqlLink));
	}
	return $result;
}



/**
 * notify error
*/
function notifyError($message, $errorDetail = "")
{
	$errorDetail = array(
	    "errorCode" => $errorDetail/*,
	    "errorMessage" => $message*/
	);
  	printResult(0, $message, $errorDetail);
  	die();
}

function closeSql()
{
	global $sqlLink;
	mysqli_close($sqlLink);	
}



/**
 * Print result in JSON
*/
function printResult($status, $message, $data = "")
{
	// check data
	if($data == "" || $data === null)
		$data = null;
	
	$result = array(
	    "status" => $status,
	    "message" => $message,
	    "data" => $data,
	);
	echo json_encode($result);
	die();
}


/**
 * Serialiaze content in base64 compressed string
 * example : YToyOntzOjg6InVzZXJuYW1lIjtzOjIyOiJBbnRob255IERlIEJyYWNrZWxlaXJlIjtzOjI6ImlkIjtzOjE6IjgiO30%3D
 */
function serializeBase64($array)
{
	/* USAGE  :
		$params = array(
		    "username" => $_GET["username"],
		    "id" => $_GET["id"],
		);
		$r = serializeBase64($params);
	*/
	return base64_encode(gzcompress(serialize($array))); // 
}
function unserializeBase64($string)
{
	/* USAGE : 
	$params = unserializeBase64($_GET["r"]);
	$username = $params["username"];
	$id = $params["id"];
	*/
	return unserialize(gzuncompress(base64_decode($string)));
}


/**
* -------------------------------------- ERROR HANDLER  ----------------------------------------
*/

//set error handler
set_error_handler("notifyAndSendError",E_ALL);




/**
* -------------------------------------- Mail system ----------------------------------------
*/

/*
require 'PHPMailer/PHPMailerAutoload.php'; // send mail

//Create a new PHPMailer instance
function createNewMailer()
{
	global $smtp_host, $smtp_port, $smtp_user, $smtp_pass ;

	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html'; //Ask for HTML-friendly debug output
	$mail->Host =  $smtp_host; //Set the hostname of the mail server
	$mail->Port = $smtp_port;//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$mail->SMTPSecure = 'tls';//Set the encryption system to use - ssl (deprecated) or tls
	$mail->SMTPAuth = true;//Whether to use SMTP authentication
	$mail->Username = $smtp_user; //Username to use for SMTP authentication - use full email address for gmail
	$mail->Password = $smtp_pass; // Password to use for SMTP authentication
	return $mail;
}
*/


/**
* -------------------------------------- Database connection ----------------------------------------
*/


$sqlLink = mysqli_connect($sql_host, $sql_user, $sql_pass) or die(mysqli_error()); 
mysqli_select_db($sqlLink,$sql_db) or die(mysqli_error()); 	 	
mysqli_query($sqlLink,"SET NAMES UTF8"); 

?> 
