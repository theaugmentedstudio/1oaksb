<?php

require "functions.php";

/**
* -------------------------------------- Params  ----------------------------------------
*/

$code;
if( isset($_GET["code"]) ){
	$code = $_GET["code"];
}


/**
* -------------------------------------- Get ticket information  ----------------------------------------
*/

$result = GetOrderInfos($code);

// close sql connection
closeSql();

// print success
printResult(1, "success", $result);

?>